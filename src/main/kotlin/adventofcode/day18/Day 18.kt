package adventofcode.day18


val input = "^^^^......^...^..^....^^^.^^^.^.^^^^^^..^...^^...^^^.^^....^..^^^.^.^^...^.^...^^.^^^.^^^^.^^.^..^.^"

fun main(args: Array<String>) {
    println(countSafeTiles(rows(parseRow(input), 40)))
    println(countSafeTiles(rows(parseRow(input), 400000)))
}


enum class Tile {
    SAFE, TRAP
}

typealias Row = List<Tile>

fun countSafeTiles(rows: Sequence<Row>) = rows.fold(0) { acc, row -> acc + countSafeTiles(row) }

fun countSafeTiles(row: Row): Int = row.fold(0) { acc, tile -> if (tile == Tile.TRAP) acc else acc + 1 }

fun nextRow(prevRow: Row): Row {
    return (0 until prevRow.size).map { index ->
        val left = prevRow[index - 1, Tile.SAFE]
        val center = prevRow[index, Tile.SAFE]
        val right = prevRow[index + 1, Tile.SAFE]
        when {
            left == Tile.TRAP && center == Tile.TRAP && right == Tile.SAFE -> Tile.TRAP
            center == Tile.TRAP && right == Tile.TRAP && left == Tile.SAFE -> Tile.TRAP
            left == Tile.TRAP && center == Tile.SAFE && right == Tile.SAFE -> Tile.TRAP
            right == Tile.TRAP && left == Tile.SAFE && center == Tile.SAFE -> Tile.TRAP
            else -> Tile.SAFE
        }
    }
}

fun rows(start: Row, totalRows: Int): Sequence<Row> = rows(start).take(totalRows)

fun rows(start: Row): Sequence<Row> = generateSequence(start) { prevRow ->
    nextRow(prevRow)
}

fun parseRow(input: String): Row = input.map { if (it == '^') Tile.TRAP else Tile.SAFE }

operator fun Row.get(position: Int, default: Tile) = if (position in 0 until this.size) this[position] else default