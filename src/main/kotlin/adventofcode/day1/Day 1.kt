package adventofcode.day1

var currentDirection = Direction.N
var blocksE = 0;
var blocksN = 0;

val visited = hashSetOf<Pair<Int, Int>>()

fun main(args: Array<String>) {
    input.split(", ")
        .map { Move(Turn.valueOf(it[0].toString()), it.drop(1).toInt()) }
        .forEach { (turn, blocks) ->
            currentDirection = currentDirection.turn(turn)
            currentDirection.move(blocks)
        }
    println(blocksE + blocksN)
}

data class Move(val turn: Turn, val blocks: Int)

enum class Direction {
    N, E, S, W;

    fun move(blocks: Int) {
        when(this) {
            N -> repeat(blocks) { blocksN++; recordPosition() }
            E -> repeat(blocks) { blocksE++; recordPosition() }
            S -> repeat(blocks) { blocksN--; recordPosition() }
            W -> repeat(blocks) { blocksE--; recordPosition() }
        }
    }

    private fun recordPosition() {
        if(blocksE to blocksN in visited) {
            println(blocksE + blocksN)
        } else {
            visited += blocksE to blocksN
        }
    }

    fun turn(turn: Turn) = when {
        this == N && turn == Turn.L -> W
        this == N && turn == Turn.R -> E
        this == E && turn == Turn.L -> N
        this == E && turn == Turn.R -> S
        this == S && turn == Turn.L -> E
        this == S && turn == Turn.R -> W
        this == W && turn == Turn.L -> S
        this == W && turn == Turn.R -> N
        else -> throw IllegalArgumentException()
    }
}

enum class Turn {
    L, R
}
