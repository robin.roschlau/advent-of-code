package adventofcode.day14

import adventofcode.infiniteLongs
import adventofcode.memoize
import org.apache.commons.codec.digest.DigestUtils

val input = "zpqevtbw"

fun main(args: Array<String>) {
    println(nthKey(input, 64, { s, i -> stretchedHash(s, i, 2016) }))
}

data class Hash(val salt: String, val index: Long, val hash: String)
val simpleHash = { salt: String, index: Long ->
    Hash(salt, index, DigestUtils.md5Hex(salt + index.toString()))
}.memoize()

val stretchedHash = { salt: String, index: Long, stretch: Int ->
    val hash = (0 until stretch).fold((salt + index.toString()).md5()) { it, i -> it.md5() }
    Hash(salt, index, hash)
}.memoize()

fun String.md5() = DigestUtils.md5Hex(this)

val triple = Regex("""(.)\1\1""")

fun nthKey(salt: String, n: Int, hash: (String, Long) -> Hash): Key {
    println("Searching for key ${n}")
    return infiniteLongs()
        .map { tryKey(salt, it, hash) }
        .filterNotNull()
        .onEach { println(it) }
        .drop(n - 1)
        .first()
}

data class Key(val char: String, val hash: Hash, val repetition: Hash)
fun tryKey(salt: String, index: Long, hash: (String, Long) -> Hash): Key? {
    val thisHash = hash(salt, index)
    val match = triple.find(thisHash.hash)
        ?: return null
    val char = match.groupValues[1]
    val repetition = hashes(salt, (index + 1)..(index + 1001), hash)
        .firstOrNull { it.hash.contains(char.repeat(5)) }
        ?: return null
    return Key(char, thisHash, repetition)
}

fun hashes(salt: String, indices: LongRange, hash: (String, Long) -> Hash) =
    indices.asSequence().map { index -> hash(salt, index) }