inline val Any?.discard
    get() = Unit

fun Int.binary(places: Int = 0) = Integer.toBinaryString(this).padStart(places, '0')
fun Char.hexToInt() = Integer.parseInt(this.toString(), 16)