package adventofcode.assembunny

import adventofcode.match
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async

class CpuState(
    val program: Program,
    var instructionPointer: Long,
    val registers: Registers
) {
    val nextInstruction get() = program[instructionPointer]
}

typealias Program = MutableList<String>
operator fun Program.get(lineNr: Long): String? {
    if (lineNr !in Int.MIN_VALUE..Int.MAX_VALUE) return null
    return this[lineNr.toInt()]
}
operator fun Program.set(lineNr: Long, line: String) {
    if (lineNr !in Int.MIN_VALUE..Int.MAX_VALUE) throw IllegalArgumentException()
    this[lineNr.toInt()] = line
}

class Registers(
    var a: Long = 0,
    var b: Long = 0,
    var c: Long = 0,
    var d: Long = 0
) {

    operator fun get(reg: Char) = when (reg) {
        'a' -> a
        'b' -> b
        'c' -> c
        'd' -> d
        else -> throw IllegalArgumentException()
    }

    operator fun set(reg: Char, value: Long) = when (reg) {
        'a' -> a = value
        'b' -> b = value
        'c' -> c = value
        'd' -> d = value
        else -> throw IllegalArgumentException()
    }

    fun inc(reg: Char) {
        this[reg] = this[reg] + 1
    }

    fun dec(reg: Char) {
        this[reg] = this[reg] - 1
    }

    fun valueOf(value: String) =
        if(value.length == 1 && value[0] in 'a'..'d') this[value[0]] else value.toLong()

    override fun toString() = "a=$a b=$b c=$c d=$d"
}

val INC = Regex("""inc (\w)""")
val DEC = Regex("""dec (\w)""")
val TGL = Regex("""tgl (\w)""")
val OUT = Regex("""out (\w)""")
val CPY = Regex("""cpy (\S+) (\S+)""")
val JNZ = Regex("""jnz (\S+) (\S+)""")

fun interpret(input: String, registers: Registers): Deferred<Registers> = async(CommonPool) {
    execute(CpuState(parse(input), 0, registers)).registers
}

fun parse(input: String): Program =
    input.split("\n").toMutableList()

tailrec suspend fun execute(cpuState: CpuState): CpuState {
    val instr = cpuState.nextInstruction ?: return cpuState
    var jump = 1L
    try {
        jump = instr.match(
            INC to { (_, reg) -> cpuState.registers.inc(reg[0]); null },
            DEC to { (_, reg) -> cpuState.registers.dec(reg[0]); null },
            TGL to { (_, target) -> tgl(cpuState, cpuState.registers.valueOf(target)); null },
            OUT to { (_, value) -> print(cpuState.registers.valueOf(value)); null },
            CPY to { (_, from, to) -> cpy(from, to, cpuState.registers) },
            JNZ to { (_, cond, jmp) -> jnz(cond, jmp, cpuState.registers) }
        ) ?: 1L
    } catch (e: Exception) {
        e.printStackTrace()
    }
    cpuState.instructionPointer += jump
    return execute(cpuState)
}

private fun cpy(from: String, to: String, registers: Registers): Long? {
    val toReg = to[0]
    if (from.length == 1 && from[0] in 'a'..'d') {
        val fromReg = from[0]
        registers[toReg] = registers[fromReg]
    } else {
        registers[toReg] = from.toLong()
    }
    return null
}

private fun jnz(cond: String, jmp: String, registers: Registers): Long? {
    val condValue = registers.valueOf(cond)
    if (condValue != 0L) {
        return registers.valueOf(jmp)
    }
    return null
}

private fun tgl(cpuState: CpuState, linesFromCurrent: Long) {
    val lineNr = linesFromCurrent + cpuState.instructionPointer
    val line = cpuState.program[lineNr]
        ?: return
    val newLine = when (line.take(3)) {
        "dec", "tgl" -> "inc"
        "inc" -> "dec"
        "cpy" -> "jnz"
        "jnz" -> "cpy"
        else -> throw IllegalArgumentException()
    } + line.drop(3)
    cpuState.program[lineNr] = newLine
}