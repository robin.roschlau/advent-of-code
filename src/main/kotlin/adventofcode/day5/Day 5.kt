package adventofcode.day5

import adventofcode.infiniteLongs
import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter


val md5 = MessageDigest.getInstance("MD5")
var password = mutableMapOf<Int, Char>()
val pwdString: String
    get() = (0..7).map { password[it] ?: '_' }.joinToString("")

fun main(args: Array<String>) {
    println(hash("abc", 3231929).isValid)
    infiniteLongs()
        .onEach { print("\r$pwdString $it ") }
        .map { index -> hash(input, index) }
        //.onEach { print(it.hex()) }
        .filter { it.isValid }
        .take(8)
        .onEach { password[it.hex[5].toString().toInt()] = it.hex[6] }
        .forEach { println("\rFound: ${it.hex}") }
    println(pwdString)
}

fun hash(id: String, index: Long): ByteArray {
    return md5.digest("$id$index".toByteArray())
}
inline val ByteArray.hex: String get() {
    return DatatypeConverter.printHexBinary(this)
}
inline val ByteArray.isValid: Boolean
    get() {
        return this.hex.run {
            startsWith("00000")
                && this[5] in '0'..'7'
                && password[this[5].toString().toInt()] == null
        }
    }