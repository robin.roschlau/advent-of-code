package adventofcode.day19

val input = 3018458

fun main(args: Array<String>) {
    //println(getsAllPresents(input))
    println(partTwo(input))
}

data class Elf(val number: Int, var next: Elf? = null, var prev: Elf? = null)

fun getElfCircle(number: Int): Elf {
    val elves = (1..number).associate { it to Elf(it) }
    elves.values.forEach { elf ->
        elf.next = elves[elf.number + 1] ?: elves[1]!!
        elf.prev = elves[elf.number - 1] ?: elves[number]!!
    }
    return elves[1]!!
}

fun getsAllPresents(number: Int): Int {
    var elf = getElfCircle(number)
    while (elf.next != elf) {
        elf.next = elf.next!!.next
        elf = elf.next!!
    }
    return elf.number
}

fun partTwo(number: Int): Int {
    var elf = getElfCircle(number)
    var oppositeElf = (1..number/2).fold(elf) { it, _ -> it.next!! }

    var count = number
    while (elf.next != elf) {
        val a = oppositeElf.prev!!
        val b = oppositeElf.next!!
        a.next = b
        b.prev = a
        oppositeElf = if (count % 2 == 1) b.next!! else b
        count -= 1


        elf = elf.next!!
    }
    return elf.number
}