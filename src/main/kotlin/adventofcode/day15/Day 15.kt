package adventofcode.day15

import adventofcode.infiniteLongs

val input = """
    Disc #1 has 13 positions; at time=0, it is at position 10.
    Disc #2 has 17 positions; at time=0, it is at position 15.
    Disc #3 has 19 positions; at time=0, it is at position 17.
    Disc #4 has 7 positions; at time=0, it is at position 1.
    Disc #5 has 5 positions; at time=0, it is at position 0.
    Disc #6 has 3 positions; at time=0, it is at position 1.
    Disc #7 has 11 positions; at time=0, it is at position 0.
""".trimIndent()

data class Disc(val index: Int, val positions: Int, val start: Int)

fun Disc.positionAt(time: Int) = (start + time) % positions

fun testTime(time: Int, discs: List<Disc>) =
    discs
        .map { disc -> disc.positionAt(time + disc.index) }
        .all { it == 0 }

val lineRegex = Regex("""Disc #(\d+) has (\d+) positions; at time=0, it is at position (\d+).""")
fun parseDisc(input: String): Disc {
    val (_, position, positions, start) = lineRegex.matchEntire(input)!!.groupValues
    return Disc(position.toInt(), positions.toInt(), start.toInt())
}

fun main(args: Array<String>) {
    val discs = input.lines().map(::parseDisc)
    val first = infiniteLongs()
        .first { testTime(it.toInt(), discs) }
    println(first)
}