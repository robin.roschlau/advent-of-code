package adventofcode.day12

fun main(args: Array<String>) {
    var a = 0
    var b = 0
    var c = 1
    var d = 0
    var line = 0
    val lines = listOf(
        { a = 1 },
        { b = 1 },
        { d = 26 },
        { if (c != 0) line += 2 - 1 },
        { if (1 != 0) line += 5 - 1 },
        { c = 7 },
        { d++ },
        { c-- },
        { if (c != 0) line += -2 - 1 },
        { c = a },
        { a++ },
        { b-- },
        { if (b != 0) line += -2 - 1 },
        { b = c },
        { d-- },
        { if (d != 0) line += -6 - 1 },
        { c = 16 },
        { d = 17 },
        { a++ },
        { d-- },
        { if (d != 0) line += -2 - 1 },
        { c-- },
        { if (c != 0) line += -5 - 1 }
    ).mapIndexed { i, it -> i to it }.associate { it }
    while (line <= lines.keys.max()!!) {
        lines[line]!!.invoke()
        line++
    }
    println("""
        a = $a
        b = $b
        c = $c
        d = $d
    """.trimIndent())
}