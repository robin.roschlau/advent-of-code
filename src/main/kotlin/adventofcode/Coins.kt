package adventofcode

enum class Coin(val value: Int) {
    PENNY(1),
    NICKEL(5),
    DIME(10),
    QUARTER(25)
    ;

    companion object {
        val MIN = PENNY
        val MAX = QUARTER
    }
}

typealias Change = Map<Coin, Int>

fun getNextLowerOrEqual(amount: Int) = Coin.values().sortedByDescending { it.value }.first { it.value <= amount }

fun findSmallestCombination(target: Int): Change {
    val coins = mutableListOf<Coin>()
    fun List<Coin>.total() = this.sumBy { it.value }

    while (coins.total() < target) {
        coins.add(getNextLowerOrEqual(target - coins.total()))
    }

    return coins.groupBy { it }.mapValues { it.value.size }
}

val Change.coinCount get() = this.values.sum()
val Change.total get() = this.entries.sumBy { it.key.value * it.value }

fun main(args: Array<String>) {
    val combination = ((20 * Coin.MIN.value)..(20 * Coin.MAX.value)).asSequence()
        .map { findSmallestCombination(it) }
        .first { it.coinCount == 20 }
    println(combination)
    println(combination.total)
}
