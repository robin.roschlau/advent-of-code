package adventofcode

data class BinaryTree<T>(
    val value: T,
    val left: BinaryTree<T>? = null,
    val right: BinaryTree<T>? = null
) {
    val size: Int by lazy { (left?.size ?: 0) + 1 + (right?.size ?: 0) }
}

fun <T> List<T>.toBinaryTree(): BinaryTree<T> = when {
    size == 1 -> BinaryTree(this[0])
    else -> {
        val middleIndex = size / 2
        val left = take(middleIndex).takeIf { it.isNotEmpty() }
        val right = drop(middleIndex + 1).takeIf { it.isNotEmpty() }
        BinaryTree(this[middleIndex], left?.toBinaryTree(), right?.toBinaryTree())
    }
}

fun <T> BinaryTree<T>.invert(): BinaryTree<T> =
    this.copy(left = right?.invert(), right = left?.invert())