package adventofcode.day13

import binary

val input = 1362
val start = Node(1, 1)
val target = Node(31, 39)

fun main(args: Array<String>) {
    println("Shortest Path: " + (shortestPath(input, start, target).size - 1))
    println("Reachable Locations: " + (reachable(input, start, 50)).size)
}

fun reachable(input: Int, start: Node, steps: Int): Set<Node> {
    var reached = mapOf(start to 0)
    var edge = setOf(start)

    fun Node.nextPossible() = sequenceOf(-1 to 0, 1 to 0, 0 to -1, 0 to 1)
        .map { (x, y) -> Node(this.x + x, this.y - y) }
        .filter { it.x >= 0 && it.y >= 0 }
        .filter { getSpace(it.x, it.y, input) == Space.OPEN }
        .filter { it !in reached }

    for (step in 1..steps) {
        edge = edge.flatMap { it.nextPossible().toList() }.toSet()
        reached += edge.map { it to step }
    }
    val maxX = reached.map { it.key.x }.max()!! + 1
    val maxY = reached.map { it.key.y }.max()!! + 1
    draw(maxX, maxY, input) { x, y -> when {
        Node(x, y) in reached -> reached[Node(x, y)]!!.toString().first()
        else -> null
    } }
    return reached.keys
}

fun getSpace(x: Int, y: Int, input: Int): Space =
    when ((x*x + 3*x + 2*x*y + y + y*y + input).binary().count { it == '1' } % 2) {
        0 -> Space.OPEN
        else -> Space.WALL
    }

fun draw(maxX: Int, maxY: Int, input: Int, render: (x: Int, y: Int) -> Char?) {
    draw(maxX, maxY) { x, y -> render(x, y) ?: getSpace(x, y, input).char }
}
fun draw(maxX: Int, maxY: Int, render: (x: Int, y: Int) -> Char) {
    println("   " + (0..maxX).joinToString(""))
    println((0..maxY).joinToString("\n") { y ->
        String.format("%2d ", y) + (0..maxX).map { x ->
            render(x, y)
        }.joinToString("")
    })
    println()
}

data class Node(val x: Int, val y: Int)
fun shortestPath(input: Int, start: Node, target: Node): List<Node> {
    var visited = mapOf(start to listOf(start))
    var edge = visited

    fun Node.nextPossible() = sequenceOf(-1 to 0, 1 to 0, 0 to -1, 0 to 1)
        .map { (x, y) -> Node(this.x + x, this.y - y) }
        .filter { getSpace(it.x, it.y, input) == Space.OPEN }
        .filter { it !in visited }

    fun draw() {
        val edgeNodes = edge.keys.asSequence()
        val maxX = edgeNodes.map { it.x }.max()!! + 1
        val maxY = edgeNodes.map { it.y }.max()!! + 1
        draw(maxX, maxY, input) { x, y -> when {
            Node(x, y) == target -> 'X'
            Node(x, y) in edge -> '+'
            Node(x, y) in visited -> '*'
            else -> null
        } }
    }

    while (target !in visited) {
        val closest = edge.minBy { it.value.size }!!
        val newEdge = closest.key.nextPossible().associate { node -> node to closest.value.plus(node) }
        edge += newEdge
        visited += newEdge
        edge -= closest.key
        draw()
    }

    val path = visited[target]!!
    val maxX = path.map { it.x }.max()!! + 1
    val maxY = path.map { it.y }.max()!! + 1
    draw(maxX, maxY, input) { x, y -> when {
        Node(x, y) in path -> 'O'
        else -> null
    } }
    return path
}

enum class Space(val char: Char) {
    OPEN(' '),
    WALL('#')
}