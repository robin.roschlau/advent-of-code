package adventofcode

fun <P1, R> ((P1) -> R).memoize(): (P1) -> R {
    val cache = mutableMapOf<P1, R>()
    return { p1 ->
        cache[p1] ?: this(p1).also { result -> cache[p1] = result }
    }
}

fun <P1, P2, R> ((P1, P2) -> R).memoize(): (P1, P2) -> R {
    val cache = mutableMapOf<Pair<P1, P2>, R>()
    return { p1, p2 ->
        cache[p1 to p2] ?: this(p1, p2).also { result -> cache[p1 to p2] = result }
    }
}
fun <P1, P2, P3, R> ((P1, P2, P3) -> R).memoize(): (P1, P2, P3) -> R {
    val cache = mutableMapOf<Triple<P1, P2, P3>, R>()
    return { p1, p2, p3 ->
        val args = Triple(p1, p2, p3)
        cache[args] ?: this(p1, p2, p3).also { result -> cache[args] = result }
    }
}

operator fun <P1, P2, P3, R> ((P1, P2, P3) -> R).invoke(p1: P1, p2: P2) =
    { p3: P3 -> this(p1, p2, p3) }

operator fun <P1, P2, R> ((P1, P2) -> R).invoke(p1: P1) =
    { p2: P2 -> this(p1, p2) }