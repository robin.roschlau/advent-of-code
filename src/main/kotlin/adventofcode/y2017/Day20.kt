package adventofcode.y2017

import adventofcode.combinations
import java.io.File
import java.util.*
import kotlin.math.abs
import kotlin.math.sign
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    println(measureTimeMillis {
        println(Day20.part1(Day20.input))
        println(Day20.part2(Day20.input))
    })
}

object Day20 {
    fun part1(input: List<Particle>): Int {
        return input.indexOf(input.minBy { it.acceleration.manhattanLength })
    }

    fun part2(input: List<Particle>): Int {
        data class ParticlePair(val p1: Particle, val p2: Particle, var lastDistance: Long = Long.MAX_VALUE) {
            override fun equals(other: Any?) = other is ParticlePair && p1 == other.p1 && p2 == other.p2
            override fun hashCode() = Objects.hash(p1, p2)
        }
        val pairs = input.combinations().map { (p1, p2) -> ParticlePair(p1, p2) }

        val tracked = pairs.toHashSet()
        val alive = pairs.flatMap { listOf(it.p1, it.p2) }.toHashSet()
        val dead = hashSetOf<Particle>()

        while (tracked.isNotEmpty()) {
            for (pair in tracked.toList()) {
                val (p1, p2) = pair
                val newDistance = p1 distanceTo p2
                if (newDistance == 0L) {
                    dead += setOf(p1, p2)
                } else if (newDistance > pair.lastDistance && !p1.isTurning && !p2.isTurning) {
                    tracked.remove(pair)
                }
                pair.lastDistance = newDistance
            }
            alive.removeIf { it in dead }
            tracked.removeIf { (p1, p2) -> p1 in dead || p2 in dead }
            alive.forEach { it.tick() }
        }
        return alive.size
    }

    data class Vector(val x: Long, val y: Long, val z: Long) {

        val manhattanLength get() = setOf(x, y, z).map(::abs).sum()
        infix fun distanceTo(other: Vector) = (this - other).manhattanLength

        operator fun plus(other: Vector) = Vector(this.x + other.x, this.y + other.y, this.z + other.z)
        operator fun minus(other: Vector) = Vector(this.x - other.x, this.y - other.y, this.z - other.z)
    }

    class Particle(var position: Vector, var velocity: Vector, val acceleration: Vector) {

        val isTurning get() = setOf(
            velocity.x to acceleration.x,
            velocity.y to acceleration.y,
            velocity.z to acceleration.z
        ).any { (v, a) -> a != 0L && v.sign != a.sign }

        fun tick() {
            this.velocity += this.acceleration
            this.position += this.velocity
        }

        infix fun distanceTo(other: Particle) = this.position distanceTo other.position
    }

    val input by lazy { parse(rawInput) }

    val particleRegex = Regex("""p=<(.+?),(.+?),(.+?)>, v=<(.+?),(.+?),(.+?)>, a=<(.+?),(.+?),(.+?)>""")
    fun parse(rawInput: List<String>) = rawInput.map { line ->
        val groups = particleRegex.matchEntire(line)!!.groupValues.drop(1).map { it.trim().toLong() }
        Particle(
            position = Vector(groups[0], groups[1], groups[2]),
            velocity = Vector(groups[3], groups[4], groups[5]),
            acceleration = Vector(groups[6], groups[7], groups[8])
        )
    }

    private val rawInput by lazy { File(Day19::class.java.getResource("/y2017/20.txt").file).readLines() }
}