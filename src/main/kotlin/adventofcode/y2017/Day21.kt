package adventofcode.y2017

import java.io.File
import java.util.*

fun main(args: Array<String>) {
    println(Day21.part1(Day21.input, 5))
    println(Day21.part1(Day21.input, 18))
}

object Day21 {
    fun part1(input: List<Rule>, iterations: Int): Int {
        return (0 until iterations).fold(Picture()) { pic, _ ->
            pic.enhance(input)
        }.switchedOnPixels
    }

    data class Rule(val base: Picture, val subst: Picture)

    data class Picture(
        val pixels: Array<Array<Boolean>> = arrayOf(
            arrayOf(false, true, false),
            arrayOf(false, false, true),
            arrayOf(true, true, true)
        )
    ) {
        init {
            assert(pixels.all { it.size == pixels.size })
        }

        val size = pixels.size
        val switchedOnPixels by lazy { pixels.sumBy { row -> row.count { it } } }

        val symmetries by lazy {
            setOf(
                this, rotated(), rotated().rotated(), rotated().rotated().rotated(),
                flippedH(), flippedV(), flippedD1(), flippedD2()
            )
        }

        fun enhance(rules: List<Rule>): Picture {
            val enhancedTiles = tiled().deepMap { tile ->
                rules.find { tile in it.base.symmetries }!!.subst
            }
            return fromTiles(enhancedTiles)
        }

        fun tiled(): Array2D<Picture> = if (size % 2 == 0) tiled(2) else tiled(3)
        private fun tiled(tileSize: Int): Array2D<Picture> {
            assert(size % tileSize == 0)
            val tileGridSize = size / tileSize
            return SquareArray(tileGridSize) { tileY, tileX ->
                Picture(SquareArray(tileSize) { y, x ->
                    pixels[tileY * tileSize + y][tileX * tileSize + x]
                })
            }
        }

        private fun rotated() = Picture(size) { x, y -> pixels[size - x - 1][y] }
        private fun flippedH() = Picture(size) { x, y -> pixels[y][size - x - 1] }
        private fun flippedV() = Picture(size) { x, y -> pixels[size - y - 1][x] }
        private fun flippedD1() = rotated().flippedV()
        private fun flippedD2() = rotated().flippedH()

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Picture

            if (!Arrays.deepEquals(pixels, other.pixels)) return false

            return true
        }

        override fun hashCode(): Int {
            return Arrays.deepHashCode(pixels)
        }

        override fun toString() =
            pixels.joinToString("\n") { it.joinToString("") { if (it) "#" else "." } }

        companion object {
            operator fun invoke(size: Int, init: (x: Int, y: Int) -> Boolean) =
                Picture(SquareArray(size) { y, x -> init(x, y) })

            fun fromTiles(tiles: Array2D<Picture>): Picture {
                val tileSize = tiles.first().first().size
                fun tile(i: Int) = i / tileSize
                fun tilePixel(i: Int) = i % tileSize
                return Picture(tiles.size * tileSize) { x, y ->
                    tiles[tile(y)][tile(x)].pixels[tilePixel(y)][tilePixel(x)]
                }
            }

            fun fromString(string: String): Picture {
                val rows = string.split("/")
                assert(rows.size in 2..4)
                assert(rows.all { it.length == rows.size })
                return Picture(rows.size) { x, y -> rows[y][x] == '#' }
            }
        }
    }

    inline fun <reified T> SquareArray(size: Int, init: (outer: Int, inner: Int) -> T) =
        Array(size) { i -> Array(size) { j -> init(i, j) } }

    val input get() = parse(rawInput)
    fun parse(rawInput: List<String>) = rawInput
        .map { line ->
            val (baseString, substString) = line.split(" => ")
            Rule(base = Picture.fromString(baseString), subst = Picture.fromString(substString))
        }

    private val rawInput by lazy { File(Day21::class.java.getResource("/y2017/21.txt").file).readLines() }
}

typealias Array2D<T> = Array<Array<T>>
inline fun <reified T, reified R> Array2D<T>.deepMap(transform: (T) -> R) =
    map { row -> row.map { transform(it) }.toTypedArray() }.toTypedArray()