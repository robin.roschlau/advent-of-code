package adventofcode.y2017

import java.io.File

fun main(args: Array<String>) {
    println(Day25.part1(Day25.input))
    // println(Day25.part2(Day25.input))
}

object Day25 {
    fun part1(machine: TuringMachine): Int {
        (0 until machine.checkAfter).forEach {
            machine.step()
        }
        return machine.checksum()
    }

    fun part2(input: Any): Any {
        TODO()
    }

    class TuringMachine(
        startStateName: String,
        val checkAfter: Long,
        private val stateIndexByName: Map<String, State>
    ) {
        var tape = object : TapeCell {
            override var value = 0
            override val left = LeftCell(right = this)
            override val right = RightCell(left = this)
        }
        var state = stateIndexByName[startStateName]!!

        fun step() {
            val action = state.actions[tape.value]!!
            tape.value = action.writeValue
            tape = tape.move(action.moveInDirection)
            state = stateIndexByName[action.continueWithState]!!
        }

        fun checksum(): Int {
            return tape.snapshot().count { it.value == 1 }
        }
    }

    interface TapeCell {
        var value: Int
        val leftVisited: Boolean get() = true
        val left: TapeCell
        val rightVisited: Boolean get() = true
        val right: TapeCell

        fun snapshot(): List<TapeCell> {
            val left = generateSequence(this) { tape -> if (tape.leftVisited) tape.left else null }
            val right = generateSequence(this) { tape -> if (tape.rightVisited) tape.right else null }
            return (left.toList().reversed() + right.drop(1))
        }

        fun move(direction: Direction) = when (direction) {
            Direction.LEFT -> left
            Direction.RIGHT -> right
        }
    }

    class LeftCell(override var value: Int = 0, override val right: TapeCell) : TapeCell {

        override var leftVisited = false
        override val left: LeftCell by lazy {
            leftVisited = true
            LeftCell(right = this)
        }

        override fun toString() = this.value.toString()
    }

    class RightCell(override var value: Int = 0, override val left: TapeCell) : TapeCell {

        override var rightVisited = false
        override val right: RightCell by lazy {
            rightVisited = true
            RightCell(left = this)
        }

        override fun toString() = this.value.toString()
    }

    data class State(val name: String, val actions: Map<Int, Action>)

    data class Action(
        val writeValue: Int,
        val moveInDirection: Direction,
        val continueWithState: String
    )

    enum class Direction {
        LEFT, RIGHT;
    }

    val input get() = parse(rawInput)
    fun parse(rawInput: String): TuringMachine {
        val init = Regex("""Begin in state (\w).\s+Perform a diagnostic checksum after (\d+) steps.""")
        val (startState, checkAfter) = init.find(rawInput)!!.destructured

        val state = Regex("""In state (\w):(?:.|\n)+?Write the value (\d)\.(?:.|\n)+?Move one slot to the (\w+)\.(?:.|\n)+?Continue with state (\w)\.(?:.|\n)+?Write the value (\d)\.(?:.|\n)+?Move one slot to the (\w+)\.(?:.|\n)+?Continue with state (\w).""")

        val states = state.findAll(rawInput)
            .map { it.destructured }
            .map { (name, value0, move0, cont0, value1, move1, cont1) ->
                name to State(name, mapOf(
                    0 to Action(value0.toInt(), Direction.valueOf(move0.toUpperCase()), cont0),
                    1 to Action(value1.toInt(), Direction.valueOf(move1.toUpperCase()), cont1)
                ))
            }
            .associate { it }

        return TuringMachine(startState, checkAfter.toLong(), states)
    }

    private val rawInput by lazy { File(Day25::class.java.getResource("/y2017/25.txt").file).readText() }
}