package adventofcode.y2017

fun main(args: Array<String>) {
    println(Day15.part1(Day15.genA, Day15.genB))
    println(Day15.part2(Day15.genA, Day15.genB))
}

object Day15 {

    val bitMask = 0xFFFF

    fun part1(genA: Sequence<Int>, genB: Sequence<Int>, take: Int = 40_000_000): Int {
        return genA.zip(genB)
            .take(take)
            .count { (a, b) -> a and bitMask == b and bitMask }
    }

    fun part2(genA: Sequence<Int>, genB: Sequence<Int>, take: Int = 5_000_000): Int {
        return genA.filter { it % 4 == 0 }
            .zip(genB.filter { it % 8 == 0 })
            .take(take)
            .count { (a, b) -> a and bitMask == b and bitMask }
    }

    val genA = generator(634, 16807)
    val genB = generator(301, 48271)

    fun generator(startsWith: Long, factor: Long) = generateSequence(startsWith) { prev ->
        (prev * factor) % Int.MAX_VALUE
    }.drop(1).map { it.toInt() }
}