package adventofcode.y2017

import java.io.File

fun main(args: Array<String>) {
    println(Day24.part1(Day24.input))
    println(Day24.part2(Day24.input))
}

object Day24 {
    fun part1(input: List<Component>): Int {
        val strongestBridge = strongestBridge(input, 0)
        println(strongestBridge)
        return strongestBridge.sumBy { it.port1 + it.port2 }
    }

    fun strongestBridge(available: List<Component>, startWith: Int): List<Component> {
        val possibleNext = available.filter { it.port1 == startWith || it.port2 == startWith }
        if (possibleNext.isEmpty()) {
            return possibleNext
        }
        return possibleNext
            .map { listOf(it) + strongestBridge(available - it, it.oppositePort(startWith)) }
            .maxBy { it.sumBy { it.port1 + it.port2 } }!!
    }

    fun part2(input: List<Component>): Int {
        val longestBridge = longestBridge(input, 0)
        println(longestBridge)
        return longestBridge.sumBy { it.port1 + it.port2 }
    }

    fun longestBridge(available: List<Component>, startWith: Int): List<Component> {
        val possibleNext = available.filter { it.port1 == startWith || it.port2 == startWith }
        if (possibleNext.isEmpty()) {
            return possibleNext
        }
        val possibleNextBridges = possibleNext
            .map { listOf(it) + longestBridge(available - it, it.oppositePort(startWith)) }
        val maxLength = possibleNextBridges.map { it.size }.max()!!
        return possibleNextBridges
            .filter { it.size == maxLength }
            .maxBy { it.sumBy { it.port1 + it.port2 } }!!
    }

    class Component(val port1: Int, val port2: Int) {
        override fun toString() = "$port1/$port2"
    }
    fun Component.oppositePort(port: Int) = if (port1 == port) port2 else port1

    val input get() = parse(rawInput)
    fun parse(rawInput: List<String>) = rawInput
        .map { it.split("/").map { it.toInt() }.let { (a, b) -> Component(a, b) } }

    private val rawInput by lazy { File(Day24::class.java.getResource("/y2017/24.txt").file).readLines() }
}