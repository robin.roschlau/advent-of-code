package adventofcode.y2017

import adventofcode.infiniteInts
import kotlin.math.max

fun main(args: Array<String>) {
    println(Day13.part1(Day13.input))
    println(Day13.part2(Day13.input))
}

object Day13 {
    fun part1(input: List<Layer>) =
        input.filter { it.positionAt(it.depth) == 0 }.sumBy { it.depth * it.range }

    fun part2(input: List<Layer>): Int = infiniteInts(startAt = 0)
        .first { shift -> input.none { it.positionAt(it.depth + shift) == 0 } }

    private fun Layer.positionAt(picosec: Int) =
        (picosec % ((range) * 2 - 2)).let { cyclePos -> cyclePos - max(0, cyclePos - (range - 1)) * 2 }

    data class Layer(val depth: Int, val range: Int)

    val input by lazy { parse(rawInput) }
    fun parse(rawInput: String): List<Layer> = rawInput.lines()
        .map { it.split(": ") }
        .map { (depth, range) -> Layer(depth.toInt(), range.toInt()) }

    private val rawInput: String = """
0: 3
1: 2
2: 4
4: 4
6: 5
8: 6
10: 6
12: 8
14: 6
16: 6
18: 9
20: 8
22: 8
24: 8
26: 12
28: 8
30: 12
32: 12
34: 12
36: 10
38: 14
40: 12
42: 10
44: 8
46: 12
48: 14
50: 12
52: 14
54: 14
56: 14
58: 12
62: 14
64: 12
66: 12
68: 14
70: 14
72: 14
74: 17
76: 14
78: 18
84: 14
90: 20
92: 14
    """.trimIndent()
}