package adventofcode.y2017

import adventofcode.*
import java.io.File

fun main(args: Array<String>) {
    println(Day22.part1(Day22.input))
    println(Day22.part2(Day22.input))
}

object Day22 {
    fun part1(input: Grid2D<NodeState>, bursts: Int = 10000, print: Boolean = false): Int {
        val carrier = Carrier(input.center())
        fun print() {
            if (print) {
                input.print(carrier.position)
                println()
            }
        }
        print()
        repeat(bursts) {
            carrier.burstV1(input)
            print()
        }
        return carrier.infectedNodes
    }

    fun part2(input: Grid2D<NodeState>, bursts: Int = 10000000): Int {
        val carrier = Carrier(input.center())
        repeat(bursts) {
            carrier.burstV2(input)
        }
        return carrier.infectedNodes
    }

    class Carrier(
        var position: Coord2D,
        private var direction: Direction = Direction.UP
    ) {
        var infectedNodes = 0

        fun burstV1(grid: Grid2D<NodeState>) {
            turn(grid[position])
            if (grid.toggle(position) == NodeState.Infected) {
                infectedNodes++
            }
            move()
        }

        fun burstV2(grid: Grid2D<NodeState>) {
            turn(grid[position])
            if (grid.next(position) == NodeState.Infected) {
                infectedNodes++
            }
            move()
        }

        private fun turn(nodeState: NodeState) {
            direction = when (nodeState) {
                NodeState.Infected -> direction.turnRight()
                NodeState.Clean -> direction.turnLeft()
                NodeState.Flagged -> direction.turnLeft().turnLeft()
                else -> direction
            }
        }

        private fun move() {
            position = position.move(direction)
        }
    }

    enum class NodeState(val char: Char) {
        Clean('.'),
        Weakened('W'),
        Infected('#'),
        Flagged('F');

        fun toggle() = when (this) {
            Clean -> Infected
            Infected -> Clean
            else -> this
        }

        fun next() = when (this) {
            Clean -> Weakened
            Weakened -> Infected
            Infected -> Flagged
            Flagged -> Clean
        }

        override fun toString() = char.toString()
    }
    fun Grid2D<NodeState>.toggle(coord: Coord2D): NodeState {
        val newValue = this[coord].toggle()
        this[coord] = newValue
        return newValue
    }
    fun Grid2D<NodeState>.next(coord: Coord2D): NodeState {
        val newValue = this[coord].next()
        this[coord] = newValue
        return newValue
    }

    val input get() = parse(rawInput)
    fun parse(rawInput: String) = rawInput
        .toGrid(NodeState.Clean) { char -> when(char) {
            '.' -> NodeState.Clean
            '#' -> NodeState.Infected
            else -> throw IllegalArgumentException()
        } }

    private val rawInput by lazy { File(Day22::class.java.getResource("/y2017/22.txt").file).readText() }
}