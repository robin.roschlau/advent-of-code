package adventofcode.y2017

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) {
    println(Day18.part1(Day18.input))
    println(Day18.part2(Day18.input))
}

object Day18 {

    fun part1(input: List<String>): Long = runBlocking {
        // Not my original solution, this was retrofitted to work with the part 2 solution
        val sounds = Channel<Long>()
        var lastSound = 0L
        val program = Program(0, input, sounds, Channel())
        program.run()
        while (!program.isReceiving) {
            delay(100)
            sounds.poll()?.let { lastSound = it }
        }
        return@runBlocking lastSound
    }

    fun part2(input: List<String>): Long = runBlocking {
        val channel0 = Channel<Long>(capacity = Channel.UNLIMITED)
        val channel1 = Channel<Long>(capacity = Channel.UNLIMITED)
        val program0 = Program(0, input, send = channel0, receive = channel1)
        val program1 = Program(1, input, send = channel1, receive = channel0)
        program0.run()
        program1.run()
        val deadlock = { channel0.isEmpty && channel1.isEmpty && program0.isReceiving && program1.isReceiving }
        while (!deadlock()) delay(100)
        return@runBlocking program1.sent
    }

    class Program(
        val id: Int,
        val code: List<String>,
        val send: SendChannel<Long>,
        val receive: ReceiveChannel<Long>
    ) {
        var isReceiving = false
        var sent = 0L

        private val compiledCode = code.map(this::compile)
        private val registers = mutableMapOf('p' to id.toLong())
        private var nextLine = 0

        fun run() = launch(CommonPool) {
            while (true) {
                compiledCode[nextLine]()
                nextLine++
            }
        }

        fun compile(line: String): suspend () -> Unit {
            val split = line.split(" ")
            val action = split[0]
            val x = split[1]
            val y = split.getOrNull(2)
            return when (action) {
                "snd" -> ({ send(x) })
                "set" -> ({ registers[x[0]] = valueOf(y!!) })
                "add" -> ({ registers[x[0]] = valueOf(x[0]) + valueOf(y!!) })
                "mul" -> ({ registers[x[0]] = valueOf(x[0]) * valueOf(y!!) })
                "mod" -> ({ registers[x[0]] = valueOf(x[0]) % valueOf(y!!) })
                "rcv" -> ({ receive(x[0]) })
                "jgz" -> ({ if (valueOf(x) > 0) nextLine += valueOf(y!!).toInt() - 1 })
                else -> throw IllegalArgumentException()
            }
        }

        suspend fun send(value: String) {
            send.send(valueOf(value))
            sent++
        }

        suspend fun receive(register: Char) {
            isReceiving = true
            registers[register] = receive.receive()
            isReceiving = false
        }

        fun valueOf(value: String): Long {
            return if (value[0] in 'a'..'z') {
                registers[value[0]] ?: 0
            } else {
                value.toLong()
            }
        }
        fun valueOf(register: Char): Long {
            return registers[register] ?: 0
        }

        override fun toString(): String {
            return "Program $id ${listOf(nextLine, isReceiving, sent)}"
        }
    }

    val input by lazy { parse(rawInput) }
    fun parse(rawInput: String) = rawInput.lines()

    private val rawInput: String = """
set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 952
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19
    """.trimIndent()
}
