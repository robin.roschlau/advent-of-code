package adventofcode.y2017

import adventofcode.infiniteInts

fun main(args: Array<String>) {
    println(solve(input))
}

fun solve(input: Input): Pair<Int, Int> {
    // Save all encountered configurations in a map together with the cycle they were encountered on
    var knownConfigs = mapOf(input to 0)
    // Keep track of last encountered configuration
    var lastConfig: Input = input
    for(cycle in infiniteInts(1)) {
        // Calculate next configuration
        lastConfig = reallocationCycle(lastConfig)
        // If that one was seen before, return the current cycle and offset to the first occurance
        knownConfigs[lastConfig]?.let { return cycle to cycle - it }
        knownConfigs += lastConfig to cycle
    }
    // This should never happen, only there to make the compiler happy
    throw IllegalStateException()
}

fun reallocationCycle(input: Input): Input {
    val local = input.toMutableList()
    val (maxIndex, maxBlocks) = local.withIndex().maxBy { it.value }!!
    local[maxIndex] = 0
    (1..maxBlocks)
        .map { (maxIndex + it) % input.size }
        .forEach { local[it]++ }
    return local.toList()
}

private typealias Input = List<Int>
private val input: Input = """
14	0	15	12	11	11	3	5	1	6	8	4	9	1	8	4
""".trimIndent().split(Regex("""\s+""")).map { it.toInt() }