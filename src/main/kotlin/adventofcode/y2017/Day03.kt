package adventofcode.y2017

import adventofcode.*
import adventofcode.Coord2D as Coord

private val input = 265149

fun main(args: Array<String>) {
    println(manhattanDistance(c(0, 0), input.toCoord()))
    println(stressTest(input))
}

fun printCoords() {
    val minX = coordMap.map { it.value.x }.min()!!
    val maxX = coordMap.map { it.value.x }.max()!!
    val minY = coordMap.map { it.value.y }.min()!!
    val maxY = coordMap.map { it.value.y }.max()!!
    val map = coordMap.entries.associate { it.value to it.key }
    val out = (maxY downTo minY).joinToString("\n") { y ->
        (minX..maxX).joinToString(" ") { x ->
            map[c(x, y)]?.plus(1)?.toString()?.padStart(6, ' ') ?: " ".repeat(6)
        }
    }
    println(out)
}

fun stressTest(until: Int): Int {
    val filled = mutableMapOf(c(0, 0) to 1)
    return infiniteInts(startAt = 2)
        .map { it.toCoord() }
        .map { c ->
            val neighbourSum = neighbours(c).map { filled[it] }.sumBy { it ?: 0 }
            filled[c] = neighbourSum
            neighbourSum
        }
        .first { it > until }
}

fun neighbours(coord: Coord): Set<Coord> {
    return (-1..1).possiblePairings(includeSelf = true)
        .filterNot { it.first == 0 && it.second == 0 }
        .map { (x, y) -> c(coord.x + x, coord.y + y) }
        .toSet()
}

fun Square.toCoord(): Coord {
    val squareIndex = this - 1
    coordMap[squareIndex]?.let { return it }
    for (i in lastCalculatedSquareIndex until squareIndex) {
        val cur = coordMap[i]!!
        val next = with(cur) { when {
            this == c(0, 0) -> right()
            y < 0 && x >= y && x <= -y -> right()
            x > 0 && y < x -> up()
            y > 0 && -x < y -> left()
            x < 0 && -y < -x -> down()
            else -> throw IllegalStateException(cur.toString())
        }}
        coordMap[i + 1] = next
    }
    return coordMap[squareIndex]!!
}

val lastCalculatedSquareIndex get() = coordMap.keys.max() ?: 0

val coordMap = mutableMapOf<Int, Coord>(0 to c(0, 0))


typealias Square = Int
