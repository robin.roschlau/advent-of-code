package adventofcode.y2017

import java.io.File

val src = System.getProperty("user.home") + "\\Source\\adventofcode\\src"
val mainFolder = "${src}\\main\\kotlin\\adventofcode\\y2017"
val testFolder = "${src}\\test\\kotlin\\adventofcode\\y2017"
val inputFolder = "${src}\\main\\resources\\y2017"

fun setup(d: Int) {
    val day = d.toString().padStart(2, '0')
    File("${inputFolder}\\$day.txt").createNewFile()
    File("${mainFolder}\\Day$day.kt").bufferedWriter().use { out ->
        out.write("""
            package adventofcode.y2017

            import java.io.File

            fun main(args: Array<String>) {
                println(Day$day.part1(Day$day.input))
                // println(Day$day.part2(Day$day.input))
            }

            object Day$day {
                fun part1(input: Any): Any {
                    TODO()
                }

                fun part2(input: Any): Any {
                    TODO()
                }

                val input get() = parse(rawInput)
                fun parse(rawInput: List<String>) = rawInput

                private val rawInput by lazy { File(Day$day::class.java.getResource("/y2017/${day}.txt").file).readLines() }
            }
        """.trimIndent())
    }
    File("""${testFolder}\Day${day}Test.kt""").bufferedWriter().use { out ->
        out.write("""
            package adventofcode.y2017

            import org.hamcrest.MatcherAssert.assertThat
            import org.hamcrest.Matchers.equalTo
            import org.junit.Test

            class Day${day}Test {

                @Test
                fun `part 1 test case`() {
                    val input = ""${'"'}
                        TODO
                    ""${'"'}.trimIndent().lines()
                    assertThat(Day$day.part1(Day$day.parse(input)), equalTo(Any()))
                }

                @Test
                fun `part 2 test case`() {
                    val input = ""${'"'}
                        TODO
                    ""${'"'}.trimIndent().lines()
                    assertThat(Day$day.part2(Day$day.parse(input)), equalTo(Any()))
                }
            }
        """.trimIndent())
    }
}