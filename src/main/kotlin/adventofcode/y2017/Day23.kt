package adventofcode.y2017

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.io.File

fun main(args: Array<String>) {
    println(Day23.part1(Day23.input))
    println(Day23.part2())
}

object Day23 {

    fun part1(input: List<String>): Long = runBlocking {
        val program = Program(input)
        program.run().join()
        return@runBlocking program.mulCounter
    }

    fun part2(): Int {

        fun Int.divides(other: Int) = other % this == 0
        fun Int.isPrime() = (2 until this / 2).any { it.divides(this) }

        return (0..17000 step 17).map { it + 106500 }
            .count { it.isPrime() }
    }

    class Program(
        code: List<String>
    ) {
        private val compiledCode = code.map(this::compile)

        private val registers = mutableMapOf<Char, Long>()

        private var nextLine = 0

        var mulCounter = 0L

        fun run() = launch(CommonPool) {
            while (nextLine in compiledCode.indices) {
                compiledCode[nextLine]()
                nextLine++
            }
        }

        private fun compile(line: String): suspend () -> Unit {
            val split = line.split(" ")
            val action = split[0]
            val x = split[1]
            val y = split.getOrNull(2)
            return when (action) {
                "set" -> ({ registers[x[0]] = valueOf(y!!) })
                "sub" -> ({ registers[x[0]] = valueOf(x[0]) - valueOf(y!!) })
                "mul" -> ({ registers[x[0]] = valueOf(x[0]) * valueOf(y!!); mulCounter++ })
                "jnz" -> ({ if (valueOf(x) != 0L) nextLine += valueOf(y!!).toInt() - 1 })
                else -> throw IllegalArgumentException(action)
            }
        }

        private fun valueOf(value: String): Long {
            return if (value[0] in 'a'..'z') {
                registers[value[0]] ?: 0
            } else {
                value.toLong()
            }
        }
        private fun valueOf(register: Char): Long {
            return registers[register] ?: 0
        }
    }

    val input get() = parse(rawInput)
    fun parse(rawInput: List<String>) = rawInput

    private val rawInput by lazy { File(Day23::class.java.getResource("/y2017/23.txt").file).readLines() }
}