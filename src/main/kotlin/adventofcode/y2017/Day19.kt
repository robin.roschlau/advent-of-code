package adventofcode.y2017

import java.io.File

fun main(args: Array<String>) {
    println(Day19.part1and2(Day19.input))
}

object Day19 {

    fun part1and2(diagram: Map<Coordinate, Field?>): String {
        var current = diagram.entries.first { it.key.y == 0 && it.value is Field.Vertical }.key
        var direction = Direction.DOWN
        var letters = listOf<Char>()
        var steps = 0L
        while (true) {
            current = current.move(direction)
            steps += 1
            val field = diagram[current]
            when (field) {
                null -> return letters.joinToString("") + "\n" + steps
                is Field.Letter -> letters += field.letter
                is Field.Joint -> direction = direction.turnLeft()
                    .takeIf { diagram[current.move(it)] != null } ?: direction.turnRight()
            }
        }
    }

    sealed class Field() {
        object Horizontal : Field()
        object Vertical : Field()
        object Joint : Field()
        data class Letter(val letter: Char) : Field()
    }

    enum class Direction { UP, DOWN, LEFT, RIGHT;
        fun turnLeft() = when (this) {
            Direction.UP -> Direction.LEFT
            Direction.DOWN -> Direction.RIGHT
            Direction.LEFT -> Direction.DOWN
            Direction.RIGHT -> Direction.UP
        }
        fun turnRight() = when (this) {
            Direction.UP -> Direction.RIGHT
            Direction.DOWN -> Direction.LEFT
            Direction.LEFT -> Direction.UP
            Direction.RIGHT -> Direction.DOWN
        }
    }

    data class Coordinate(val x: Int, val y: Int) {
        fun move(direction: Direction) = when (direction) {
            Direction.UP -> copy(y = y - 1)
            Direction.DOWN -> copy(y = y + 1)
            Direction.LEFT -> copy(x = x - 1)
            Direction.RIGHT -> copy(x = x + 1)
        }
    }

    val input: Map<Coordinate, Field?> by lazy { parse(rawInput) }
    fun parse(rawInput: List<String>) = rawInput
        .map { it.map {  c -> when (c) {
            ' ' -> null
            '-' -> Field.Horizontal
            '|' -> Field.Vertical
            '+' -> Field.Joint
            else -> Field.Letter(c)
        } } }
        .map { it.withIndex() }
        .withIndex()
        .flatMap { line -> line.value.map { col -> Coordinate(col.index, line.index) to col.value } }
        .associate { it }

    private val rawInput by lazy { File(Day19::class.java.getResource("/y2017/19.txt").file).readLines() }
}