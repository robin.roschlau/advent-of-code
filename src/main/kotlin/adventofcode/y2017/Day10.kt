package adventofcode.y2017

import adventofcode.reverse

fun main(args: Array<String>) {
    println(Day10.part1(Day10.input))
    println(Day10.part2(Day10.input2))
}

object Day10 {
    fun part1(lengths: List<Int>): Any {
        val list = knotBase(lengths)
        return list[0] * list[1]
    }

    fun part2(input: String): Any {
        return knot(input)
    }

    fun knot(input: String, rounds: Int = 64): String =
        knotBase(input.map { it.toByte().toInt() } + listOf(17, 31, 73, 47, 23), rounds)
            .chunked(16)
            .map { it.reduce { acc, i -> acc xor i } }
            .joinToString("") { it.toString(16).padStart(2, '0') }
            .toLowerCase()

    fun knotBase(input: List<Int>, rounds: Int = 1): List<Int> {
        var list = (0..255).toList()
        var current = 0
        var skip = 0
        repeat(rounds) {
            for (number in input) {
                list = list.reverse(current, number)
                current += number + skip
                skip += 1
            }
        }
        return list
    }

    val input: List<Int> by lazy { rawInput.split(",").map { it.toInt() } }
    val input2: String by lazy { rawInput }
    private val rawInput: String = """
        106,118,236,1,130,0,235,254,59,205,2,87,129,25,255,118
    """.trimIndent()
}