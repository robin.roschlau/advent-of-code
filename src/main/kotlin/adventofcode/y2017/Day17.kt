package adventofcode.y2017

fun main(args: Array<String>) {
    println(Day17.part1(Day17.input, 2017))
    println(Day17.part2(Day17.input, 50_000_000))
}

object Day17 {
    fun part1(input: Int, values: Int): Int {
        val buffer = mutableListOf(0)
        var curPos = 0
        for (i in 1..values) {
            if (buffer.size != 0) {
                curPos = ((curPos + input) % buffer.size) + 1
            }
            buffer.add(curPos, i)
        }
        return buffer[buffer.indexOf(2017) + 1]
    }

    fun part2(input: Int, values: Int): Int {
        var bufferSize = 1
        var i1 = 0
        var curPos = 0
        for (i in 1..values) {
            curPos = ((curPos + input) % bufferSize) + 1
            if (curPos == 1) i1 = i
            bufferSize += 1
        }
        return i1
    }

    val input = 348
}