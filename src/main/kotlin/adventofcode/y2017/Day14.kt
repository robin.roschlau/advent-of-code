package adventofcode.y2017

import adventofcode.neighbours
import adventofcode.possiblePairings
import binary
import hexToInt
import adventofcode.Coord2D as Coord

fun main(args: Array<String>) {
    println(Day14.part1(Day14.input))
    println(Day14.part2(Day14.input))
}

object Day14 {
    fun part1(input: String): Int {
        return calculateGrid(input).sumBy { it.count { it } }
    }

    fun part2(input: String): Int {
        val grid = calculateGrid(input)
        var groups = setOf<Set<Coord>>()
        var visited = setOf<Coord>()
        (0..127).possiblePairings(includeSelf = true)
            .asSequence()
            .map { (x, y) -> Coord(x, y) }
            .filter { it !in visited }
            .filter { grid[it.y][it.x] }
            .map { grid.findRegions(it) }
            .forEach {
                groups += setOf(it)
                visited += it
            }
        return groups.size
    }

    fun Grid.findRegions(coord: Coord): Set<Coord> {
        var regions = setOf<Coord>()
        var edge = setOf(coord)
        while (edge.isNotEmpty()) {
            regions += edge
            edge = edge.flatMap { it.neighbours.filter { setOf(it.x, it.y).all { it in 0..127 } } }
                .filter { it !in regions }
                .filter { this[it.y][it.x] }
                .toSet()
        }
        return regions
    }

    fun calculateGrid(input: String): Grid =
        (0..127).asSequence()
            .map { "$input-$it" }
            .map { Day10.knot(it) }
            .map { hexLine -> hexLine.map { char -> char.hexToInt().binary(4) }.joinToString("") }
            .map { binLine -> binLine.map { it == '1' } }
            .toList()

    val input = "hxtvlmkl"
}

private typealias Grid = List<List<Boolean>>