package adventofcode.day10

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.channels.ActorJob
import kotlinx.coroutines.experimental.channels.actor
import kotlinx.coroutines.experimental.runBlocking


fun main(args: Array<String>) {
    runBlocking {
        val (outputs, botLog) = simulate(input)
        println(determineBot(botLog, 61, 17))
        println(outputs[0]!! * outputs[1]!! * outputs[2]!!)
    }
}

suspend fun determineBot(botLog: BotLog, in1: Int, in2: Int): Int? {
    return botLog.entries.find { (_, numbers) -> numbers == setOf(in1, in2) }?.key
}

typealias BotId = Int
typealias OutputId = Int

// A log of which bot received which numbers
typealias BotLog = Map<BotId, Set<Int>>
// A log of which output received which number
typealias OutputLog = Map<OutputId, Int>
data class SimulationResult(val outputLog: OutputLog, val botLog: BotLog)

suspend fun simulate(input: String): SimulationResult {
    // Parse input
    val rules = input.lines().map { parse(it) }
    // Create empty logs
    val outputLog = mutableMapOf<OutputId, Int>()
    val botLog = mutableMapOf<BotId, Set<Int>>()

    var bots = mapOf<BotId, ActorJob<Int>>()

    val send: suspend (numberAndReceiver: Pair<Int, RuleReceiver>) -> Unit = { (number, receiver) ->
        when (receiver) {
            is RuleReceiver.Bot -> bots[receiver.id]!!.channel.send(number)
            is RuleReceiver.Output -> outputLog[receiver.id] = number
        }
    }

    bots = rules.filterIsInstance<BotRule>().associate { rule ->
        rule.id to Bot(rule, send, { botLog[rule.id] = it})
    }

    rules.filterIsInstance<Input>().forEach {
        bots[it.feedsBot]!!.channel.send(it.number)
    }

    bots.values.forEach { it.join() }

    return SimulationResult(outputLog, botLog)
}

class Bot(
    val rule: BotRule,
    send: suspend (Pair<Int, RuleReceiver>) -> Unit,
    log: (Set<Int>) -> Unit
) : ActorJob<Int> by actor<Int>(CommonPool, block = {
    val numbers = setOf(channel.receive(), channel.receive())
    log(numbers)
    send(numbers.max()!! to rule.high)
    send(numbers.min()!! to rule.low)
})

val INPUT_PATTERN = Regex("""value (\d+) goes to bot (\d+)""")
val RULE_PATTERN = Regex("""bot (\d+) gives low to (output|bot) (\d+) and high to (output|bot) (\d+)""")

fun parse(line: String): Rule {
    return when {
        INPUT_PATTERN.matches(line) -> parseInput(line)
        else -> parseBot(line)
    }
}

fun parseBot(line: String): BotRule {
    val match = RULE_PATTERN.matchEntire(line) ?: throw NullPointerException("$line did not match rule pattern")
    val (_, bot, lowTargetType, lowTargetId, highTargetType, highTargetId) = match.groupValues
    val lowTarget = if(lowTargetType == "output") RuleReceiver.Output(lowTargetId.toInt()) else RuleReceiver.Bot(lowTargetId.toInt())
    val highTarget = if(highTargetType == "output") RuleReceiver.Output(highTargetId.toInt()) else RuleReceiver.Bot(highTargetId.toInt())
    return BotRule(bot.toInt(), low = lowTarget, high = highTarget)
}

private operator fun <E> List<E>.component6(): E {
    return this[5]
}

fun parseInput(line: String): Input {
    val match = INPUT_PATTERN.matchEntire(line)!!
    val (_, value, bot) = match.groupValues
    return Input(value.toInt(), bot.toInt())
}

sealed class Rule
data class Input(val number: Int, val feedsBot: BotId): Rule()
data class BotRule(val id: BotId, val high: RuleReceiver, val low: RuleReceiver) : Rule()

sealed class RuleReceiver {
    data class Bot(val id: BotId) : RuleReceiver()
    data class Output(val id: OutputId) : RuleReceiver()
}