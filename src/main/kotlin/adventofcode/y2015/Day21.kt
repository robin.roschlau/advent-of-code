package adventofcode.y2015

import kotlin.math.max


fun main(args: Array<String>) {
    println(Day21.part1(Day21.Boss()))
    println(Day21.part2(Day21.Boss()))
}

object Day21 {
    private val player = Equipment("Player", hitpoints = 100)

    fun part1(boss: Boss) = getCombinations(player)
        .sortedBy { it.cost }
        .first { it.winsAgainst(boss) }
        .cost

    fun part2(boss: Boss) = getCombinations(player)
        .sortedByDescending { it.cost }
        .first { !it.winsAgainst(boss) }
        .cost

    fun Equipment.winsAgainst(boss: Boss): Boolean {
        val playerDealsDamage = max(1, this.damage - boss.armor)
        val bossDealsDamage = max(1, boss.damage - this.armor)
        return boss.hitpoints / playerDealsDamage <= this.hitpoints / bossDealsDamage
    }

    private fun getCombinations(player: Equipment): Set<Equipment> {
        val weaponArmorCombinations = weapons.flatMap { weapon ->
            armor.map { armor -> listOfNotNull(player, weapon, armor) }
        }
        val ringCombinations = rings.flatMap { ring1 ->
            rings.filter { it == null || it != ring1 }.map { ring2 -> listOfNotNull(ring1, ring2) }
        }
        val all = weaponArmorCombinations.flatMap { wa ->
            ringCombinations.map { rings -> wa + rings }
        }
        return all.map { it.reduce(Equipment::plus) }.toSet()
    }

    val weapons = listOf(
        Equipment("Dagger", cost = 8 , damage = 4),
        Equipment("Shortsword", cost = 10, damage = 5),
        Equipment("Warhammer", cost = 25, damage = 6),
        Equipment("Longsword", cost = 40, damage = 7),
        Equipment("Greataxe", cost = 74, damage = 8)
    )
    val armor = listOf(null, // null because we can also pick no armor
        Equipment("Leather", cost = 13, armor = 1),
        Equipment("Chainmail", cost = 31, armor = 2),
        Equipment("Splintmail", cost = 53, armor = 3),
        Equipment("Bandedmail", cost = 75, armor = 4),
        Equipment("Platemail", cost = 102, armor = 5)
    )
    val rings = listOf(null,
        Equipment("Damage +1", cost = 25, damage = 1),
        Equipment("Damage +2", cost = 50, damage = 2),
        Equipment("Damage +3", cost = 100, damage = 3),
        Equipment("Defense +1", cost = 20, armor = 1),
        Equipment("Defense +2", cost = 40, armor = 2),
        Equipment("Defense +3", cost = 80, armor = 3)
    )

    data class Equipment(
        val name: String,
        val cost: Int = 0,
        val hitpoints: Int = 0,
        val damage: Int = 0,
        val armor: Int = 0
    ) {
        operator fun plus(other: Equipment) =
            Equipment(
                name = "${this.name}, ${other.name}",
                hitpoints = this.hitpoints + other.hitpoints,
                cost = this.cost + other.cost,
                armor = this.armor + other.armor,
                damage = this.damage + other.damage
            )
    }

    data class Boss(val hitpoints: Int = 100, val damage: Int = 8, val armor: Int = 2)

}