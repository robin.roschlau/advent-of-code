package adventofcode.y2015

import adventofcode.infiniteInts
import adventofcode.memoize
import kotlin.math.sqrt

private val input = 34000000

fun main(args: Array<String>) {
    val house = infiniteInts(0)
        .first { numberOfPresents(it) >= input }
    println(house)
}

fun numberOfPresents(house: Int): Int =
    divisors(house).fold(0) { acc, i -> acc + i*10 }

val divisors = { number: Int ->
    val maxD = sqrt(number.toDouble()).toInt()
    var divisors = setOf(1, number)
    for (i in 2..maxD) {
        if (number % i == 0) {
            divisors += i
            val d = number / i
            if (d != i)
                divisors += d
        }
    }
    divisors
}.memoize()