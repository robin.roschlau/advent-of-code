package adventofcode.day11

import adventofcode.crossproduct

fun main(args: Array<String>) {
    val roomState = parseRoomState(input2)
    println(roomState)
    println(getMinimumNumberOfSteps(roomState))
}

val FLOOR_PATTERN = Regex("The (\\w+) floor")
val MICROCHIP_PATTERN = Regex("an? (\\w+)-compatible microchip")
val GENERATOR_PATTERN = Regex("an? (\\w+) generator")

fun getMinimumNumberOfSteps(roomState: RoomState): Int {
    var states = setOf(roomState)
    var round = 0
    val visitedStates = mutableSetOf<MinState>()
    while (states.none { it.satisfiesEndCondition() }) {
        println("No solution in round $round, ${states.size} new states, ${visitedStates.size} known states")
        visitedStates.addAll(states.map { it.state })
        states = states.flatMap { it.getPossibleNextStates() }.distinctBy { it.state }.filter { it.state !in visitedStates }.toSet()
        round += 1
    }
    println("Solution found after round $round!")
    return round
}

fun RoomState.satisfiesEndCondition(): Boolean =
    elevator.floor == RoomState.topFloorIndex &&
        (generators + chips).all { it.floor == RoomState.topFloorIndex }

fun Move.isValid(): Boolean {
    return item2 == null ||
        item1.javaClass.isAssignableFrom(item2.javaClass) ||
        item1.element == item2.element
}

fun RoomState.getPossibleNextStates() =
    this.getPossibleValidMoves()
        .map { doMove(this, it) }
        .filterIsInstance<Result.Ok<RoomState>>()
        .map { it.value }
        .toList()

fun RoomState.getPossibleValidMoves(): Sequence<Move> {
    val currentFloor = this.elevator.floor
    val possibleTargetFloors = sequenceOf(1, -1).map { currentFloor + it }.filter { it in RoomState.bottomFloorIndex..RoomState.topFloorIndex }
    val itemsOnFloor = (this.chips + this.generators).filter { it.floor == currentFloor }.asSequence()
    val combinations = itemsOnFloor
        .crossproduct(sequenceOf<FloorItem?>(null) + itemsOnFloor)
        .filter { it.first != it.second }
    return possibleTargetFloors
        .flatMap { targetFloor -> combinations.map { Move(targetFloor, it.first, it.second) } }
        .filter { it.isValid() }
}

fun doMove(roomState: RoomState, move: Move): Result<RoomState> {
    if(!isLogisticallyPossible(roomState, move)) return Result.Error("Impossible move")
    if(!makesSense(roomState, move)) return Result.Error("Move is unnecessary")
    val (toFloor, element1, element2) = move
    val chips = roomState.chips.move(element1, toFloor).move(element2, toFloor)
    val gens = roomState.generators.moveG(element1, toFloor).moveG(element2, toFloor)
    val stateAfterMove = RoomState(chips.toSet(), gens.toSet(), Elevator(toFloor))
    if(!stateAfterMove.isValid()) {
        return Result.Error("Chip destroyed")
    }
    return Result.Ok(stateAfterMove)
}

fun isLogisticallyPossible(roomState: RoomState, move: Move): Boolean {
    if ((roomState.elevator.floor - move.toFloor) !in -1..1) return false
    if (roomState.elevator.floor != move.item1.floor) return false
    if (move.item2 != null && roomState.elevator.floor != move.item2.floor) return false

    val items = roomState.items
    if (move.item2 != null && !items.contains(move.item2)) return false
    if (!items.contains(move.item1)) return false
    return true
}

fun makesSense(roomState: RoomState, move: Move): Boolean {
    if (roomState.items.none { it.floor <= move.toFloor }) return false
    return true
}

fun RoomState.isValid(): Boolean {
    chips.forEach { chip ->
        val generatorsOnThisFloor = this.items.filter { it.floor == chip.floor }.filterIsInstance<Generator>()
        val hostileGenerator = generatorsOnThisFloor.any { it.element != chip.element }
        val matchingGenerator = generatorsOnThisFloor.any { it.element == chip.element }
        if (hostileGenerator && !matchingGenerator) {
            return false
        }
    }
    return true
}

sealed class Result<out T> {
    data class Ok<out T>(val value: T) : Result<T>()
    data class Error(val msg: String) : Result<Nothing>()
}

fun <T : Collection<Microchip>> T.move(item: FloorItem?, toFloor: FloorNr) =
    this.map { if (it == item) it.copy(floor = toFloor) else it }
fun <T : Collection<Generator>> T.moveG(item: FloorItem?, toFloor: FloorNr) =
    this.map { if (it == item) it.copy(floor = toFloor) else it }

data class Move(val toFloor: FloorNr, val item1: FloorItem, val item2: FloorItem?)

data class RoomState(
    val chips: Set<Microchip>,
    val generators: Set<Generator>,
    val elevator: Elevator = Elevator()
) {

    val items = chips.asSequence() + generators

    override fun toString() = (topFloorIndex downTo bottomFloorIndex)
        .joinToString("\n") { "F$it " + (if(elevator.floor == it) "E " else ". ") + printFloor(it) }

    fun printFloor(floor: FloorNr) = items
            .sortedBy { it.toString() }
            .joinToString(" ") { if(it.floor == floor) it.toString() else ". " }

    val state: MinState = MinState(
        elevator.floor,
        chips.map { chip -> chip.floor to generators.find { it.element == chip.element }!!.floor }
            .groupBy { it }
            .mapValues { it.value.size }
    )

    companion object {
        const val topFloorIndex = 4
        const val bottomFloorIndex = 1
    }
}

data class MinState(
    val elevator: FloorNr,
    val pairs: Map<Pair<FloorNr, FloorNr>, Int>
)

interface FloorItem {
    val floor: Int
    val element: String
}
data class Elevator(var floor: FloorNr = 1)
data class Microchip(override val element: String, override val floor: FloorNr) : FloorItem {
    override fun toString() = this.element.first().toUpperCase() + "M"
}
data class Generator(override val element: String, override val floor: FloorNr) : FloorItem {
    override fun toString() = this.element.first().toUpperCase() + "G"
}