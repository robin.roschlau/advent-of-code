package adventofcode.day11


typealias FloorNr = Int
private fun String.parseFloorNr(): FloorNr = when (this) {
    "first" -> 1
    "second" -> 2
    "third" -> 3
    "fourth" -> 4
    else -> throw IllegalArgumentException("Not a valid floor number: $this")
}
fun parseRoomState(input: String): RoomState {
    val (chips, generators) = input.lines()
        .map { line ->
            val floor = FLOOR_PATTERN.find(line)!!.groupValues[1].parseFloorNr()
            val chips = MICROCHIP_PATTERN.findAll(line).map { Microchip(it.groupValues[1], floor) }
            val generators = GENERATOR_PATTERN.findAll(line).map { Generator(it.groupValues[1], floor) }
            chips to generators
        }.reduce { acc, next -> acc.first + next.first to acc.second + next.second }
    val room = RoomState(chips.toSet(), generators.toSet())
    return room
}