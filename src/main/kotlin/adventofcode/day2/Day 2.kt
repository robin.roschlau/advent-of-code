package adventofcode.day2


val keypadFields = arrayOf(
    arrayOf(' ',' ','1',' ',' '),
    arrayOf(' ','2','3','4',' '),
    arrayOf('5','6','7','8','9'),
    arrayOf(' ','A','B','C',' '),
    arrayOf(' ',' ','D',' ',' ')
)

fun main(args: Array<String>) {
    var currentButton = Vector(0, 2)
    input.lines().forEach { line ->
        line.forEach { char ->
            currentButton = currentButton.move(char)
        }
        print(currentButton.button)
    }
}


data class Vector(val x: Int, val y: Int) {

    val button: Char
        get() = if (x !in 0..4 || y !in 0..4) ' ' else keypadFields[y][x]

    fun move(char: Char) =
        copy(
            x = x + if(char == 'R') 1 else if(char == 'L') -1 else 0,
            y = y + if(char == 'D') 1 else if(char == 'U') -1 else 0
        ).takeIf { it.button != ' ' } ?: this

    override fun toString() = "[$x, $y]"

    companion object {
        fun from(char: Char) = when (char) {
            'R' -> Vector(1, 0)
            'L' -> Vector(-1, 0)
            'D' -> Vector(0, 1)
            'U' -> Vector(0, -1)
            else -> throw IllegalArgumentException()
        }
    }
}