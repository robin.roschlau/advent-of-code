package adventofcode.day7

import java.io.File

val inputTest = """
abba[mnop]qrst
abcd[bddb]xyyx
aaaa[qwer]tyui
ioxxoj[asdfgh]zxcvbn
""".trim().lines()

val inputTest2 = """
aba[bab]xyz
xyx[xyx]xyx
aaa[kek]eke
zazbz[bzb]cdb
""".trim().lines()

class Help
val input by lazy { File(Help::class.java.getResource("/day7input.txt").file).readLines() }