package adventofcode.day7

import kotlin.math.max

val TLS_MUST_MATCH = Regex("\\w*?(?:(\\w)(?!\\1)(\\w)\\2\\1)\\w*?")
val TLS_MUST_NOT_MACTH = Regex("\\[\\w*?(?:(\\w)(?!\\1)(\\w)\\2\\1)\\w*?]")

fun main(args: Array<String>) {

    println(input.count {
        TLS_MUST_MATCH.containsMatchIn(it) && !TLS_MUST_NOT_MACTH.containsMatchIn(it)
    })

    val result = input.count {
        val (supernet, hypernet) = it.getSequences()
        supernet.any { supSeq ->
            supSeq.getAbas().any { aba ->
                hypernet.any { hypSeq ->
                    hypSeq.hasBab(aba)
                }
            }
        }
    }

    println(result)
}

private fun String.getSequences(): Pair<List<String>, List<String>> {
    var supernet = listOf<String>()
    var hypernet = listOf<String>()
    var hypernetLevel = 0
    var current = StringBuilder()
    fun finish() {
        when (hypernetLevel) {
            0 -> supernet += current.toString()
            else -> hypernet += current.toString()
        }
        current = StringBuilder()
    }
    for (char in this) {
        when (char) {
            '[' -> {
                finish()
                hypernetLevel++
            }
            ']' -> {
                finish()
                hypernetLevel = max(hypernetLevel - 1, 0)
            }
            else -> current.append(char)
        }
    }
    finish()
    return Pair(supernet, hypernet)
}

private fun String.getAbas(): List<String> {
    val aba = Regex("(\\w)(?!\\1)(\\w)\\1")
    val abas = aba.findAllOverlapping(this).map { it.value }.toList()
    return abas
}

fun Regex.findAllOverlapping(input: CharSequence, startIndex: Int = 0) =
    generateSequence({ find(input, startIndex) }) { find(input, it.range.first + 1) }

private fun String.hasBab(aba: String): Boolean {
    val bab = String(charArrayOf(aba[1], aba[0], aba[1]))
    return bab in this
}

