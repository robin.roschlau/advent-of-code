package adventofcode.day6

fun main(args: Array<String>) {
    val lineLength = testInput.lines().first().length
    val message = mutableMapOf<Int, MutableList<Char>>()
    input.lines().forEach { line ->
        line.forEachIndexed { index, c ->
            val chars = message[index] ?: mutableListOf<Char>().also { message[index] = it }
            chars += c
        }
    }
    val result = message.entries
        .sortedBy { entry -> entry.key }
        .map { entry -> entry.value }
        .map { chars -> chars.associate { char -> char to chars.count { it == char } }.minBy { it.value }?.key }
        .joinToString("")
    println(result)
}