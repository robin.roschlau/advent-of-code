package adventofcode.day9

fun main(args: Array<String>) {
    println(countV2(input))
}

val MARKER_START_PATTERN = Regex("""^\((\d+)x(\d+)\)""")
fun decompressV1(input: String): String {
    var rest = input
    var out = StringBuilder()

    while (rest.length > 0) {
        val match = MARKER_START_PATTERN.find(rest)
        if (match != null) {
            val (marker, chars, repetitions) = match.groupValues
            rest = rest.drop(marker.length)
            val group = rest.take(chars.toInt())
            rest = rest.drop(chars.toInt())
            out.append(group.repeat(repetitions.toInt()))
        } else {
            out.append(rest[0])
            rest = rest.drop(1)
        }
    }

    return out.toString()
}

fun countV2(input: String): Long {
    val area = RepeatedArea(getAreasV2(input), 1)
    return area.length
}

val MARKER_PATTERN = Regex("""\((\d+)x(\d+)\)""")

fun getAreasV2(input: String): List<Area> {
    var rest = input
    var out = listOf<Area>()

    while (rest.length > 0) {
        val match = MARKER_PATTERN.find(rest)
        if (match != null) {
            val (marker, chars, repetitions) = match.groupValues
            val firstMatchIndex = match.range.first
            if(firstMatchIndex != 0) {
                out += RawArea(rest.take(firstMatchIndex))
                rest = rest.drop(firstMatchIndex)
            }
            rest = rest.drop(marker.length)
            out += RepeatedArea(getAreasV2(rest.take(chars.toInt())), repetitions.toLong())
            rest = rest.drop(chars.toInt())
        } else {
            return out + RawArea(rest)
        }
    }

    return out
}

sealed class Area {
    abstract val length: Long
}
class RawArea(val area: String): Area() {
    override val length = area.length.toLong()
    override fun toString() = area
}
class RepeatedArea(val areas: List<Area>, val repeat: Long): Area() {
    override val length = areas.fold(0L) { totalLength, area -> totalLength + area.length } * repeat
    override fun toString() = "$repeat x $areas"
}