package adventofcode.day17

import adventofcode.day14.md5
import adventofcode.memoize


data class Room(val x: Int, val y: Int)

val start = Room(0, 0)
val vault = Room(3, 3)

val input = "vkjiggvb"

fun main(args: Array<String>) {
    println("Shortest Path: " + shortestPath(input))
    println("Longest Path: " + longestPath(input).length)
}

fun shortestPath(passcode: String): String {
    var paths = setOf("")
    var visitedRooms = setOf(start)
    while (vault !in visitedRooms) {
        paths = paths
            .flatMap { path -> openDoors(passcode, path).map { path + it } }
            .toSet()
        visitedRooms += paths.map(room)
    }
    return paths.first { vault == room(it) }
}

fun longestPath(passcode: String): String {
    var paths = setOf("")
    var longestPath = ""
    while (paths.isNotEmpty()) {
        longestPath = paths.firstOrNull { path -> room(path) == vault } ?: longestPath
        paths = paths
            .flatMap { path -> openDoors(passcode, path).map { path + it } }
            .toSet()
    }
    return longestPath
}

fun openDoors(passcode: String, path: String): Set<Char> = room(path).let { room ->
    when {
        room == vault -> setOf()
        else -> (passcode + path).md5()
            .take(4)
            .zip("UDLR")
            .filter { it.first in "bcdef" }
            .map { it.second }
            .filter { it in doors(room(path)) }
            .toSet()
    }
}

val doors = { room: Room ->
    mapOf<Char, (Room) -> Boolean>(
        'U' to { room: Room -> room.y > 0 },
        'D' to { room: Room -> room.y < 3 },
        'L' to { room: Room -> room.x > 0 },
        'R' to { room: Room -> room.x < 3 }
    ).filter { it.value(room) }.keys
}.memoize()

val room = { path: String ->
    path.fold(start) { room, char -> when (char) {
        'U' -> room.copy(y = room.y - 1)
        'D' -> room.copy(y = room.y + 1)
        'L' -> room.copy(x = room.x - 1)
        'R' -> room.copy(x = room.x + 1)
        else -> throw IllegalArgumentException()
    }}
}.memoize()