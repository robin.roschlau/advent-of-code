package adventofcode

infix fun String.shift(n: Int) = when {
    // Shift right
    n > 0 -> this.takeLast(n) + this.dropLast(n)
    // Shift left
    n < 0 -> this.drop(-n) + this.take(-n)
    // No shift
    else -> this
}

fun String.swap(i1: Int, i2: Int): String {
    val arr = this.toCharArray()
    val c1 = arr[i1]
    arr[i1] = arr[i2]
    arr[i2] = c1
    return String(arr)
}

inline fun <reified T> String.match(vararg cases: Pair<Regex, (List<String>) -> T>): T =
    cases.asSequence()
        .map { entry -> entry.first.matchEntire(this)?.let { match -> match to entry.second } }
        .filterNotNull()
        .first()
        .let { (match, callback) -> callback(match.groupValues) }
