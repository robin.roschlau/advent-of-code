package adventofcode


fun <T, U> Sequence<T>.crossproduct(other: Sequence<U>) =
    this.flatMap { first -> other.map { second -> first to second } }
fun <T, U> Iterable<T>.crossproduct(other: Iterable<U>) =
    this.flatMap { first -> other.map { second -> first to second } }

fun infiniteLongs(startAt: Long = 0) = generateSequence(startAt) { i -> i + 1 }
fun infiniteInts(startAt: Int = 0) = generateSequence(startAt) { i -> i + 1 }

fun <E> List<E>.wrappedIndex(index: Int): Int {
    var i = index
    while (i < 0) i += size
    return i % size
}

operator fun <E> List<E>.get(range: IntRange) = this.slice(range)

fun <E> List<E>.reverse(start: Int, length: Int): List<E> {
    if (length > this.size) throw IllegalArgumentException()
    if (length == 0) return this
    val indices = (start until start + length).map { wrappedIndex(it) }
    val sublist = this.slice(indices).reversed()
    return this.mapIndexed { index, e -> when {
        index in indices -> sublist[indices.indexOf(index)]
        else -> e
    }}
}

fun <K, V> MutableMap<K, V>.withDefault(default: V, addOnGet: Boolean = false) =
    DefaultedMapImpl(default, this, addOnGet)

interface DefaultedMap<K, V> : MutableMap<K, V> {
    override fun get(key: K): V
}

class DefaultedMapImpl<K, V>(
    val default: V,
    val map: MutableMap<K, V> = mutableMapOf(),
    val addOnGet: Boolean = false
): MutableMap<K, V> by map, DefaultedMap<K, V> {
    override fun get(key: K): V = map.get(key) ?: default.also { if (addOnGet) map.put(key, it) }
}