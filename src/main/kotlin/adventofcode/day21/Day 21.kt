package adventofcode.day21

import adventofcode.invoke
import adventofcode.match
import adventofcode.permutations

fun main(args: Array<String>) {
    val scrambled = input.lines()
        .map { parse(it) }
        .fold("abcdefgh") { pwd, operation -> operation(pwd) }
    println(scrambled)

    val pwd = input.lines()
        .reversed()
        .map { parseReverse(it) }
        .fold("fbgdceah") { pwd, operation -> operation(pwd) }
    println(pwd)

    // Alternatively, brute force part two:
    println("fbgdceah".toSet()
        .permutations()
        .map { it.joinToString("") }
        .first {
            input.lines()
                .map { parse(it) }
                .fold(it) { pwd, operation -> operation(pwd) } == "fbgdceah"
        })
}


typealias Operation = (String) -> String


val SWAP_POSITION = Regex("""swap position (\d+) with position (\d+)""")
val SWAP_LETTER = Regex("""swap letter (\w) with letter (\w)""")
val ROTATE = Regex("""rotate (left|right) (\d+) steps?""")
val ROTATE_LETTER = Regex("""rotate based on position of letter (\w)""")
val REVERSE_SPAN = Regex("""reverse positions (\d+) through (\d+)""")
val MOVE = Regex("""move position (\d+) to position (\d+)""")


fun parse(str: String): Operation = str.match(
    SWAP_POSITION to { (_, x, y) ->
        (::swapPosition)(x.toInt(), y.toInt())
    },
    SWAP_LETTER to { (_, x, y) ->
        (::swapLetters)(x, y)
    },
    ROTATE to { (_, direction, steps) ->
        (::rotate)(direction, steps.toInt())
    },
    ROTATE_LETTER to { (_, x) ->
        (::rotateLetter)(x)
    },
    REVERSE_SPAN to { (_, x, y) ->
        (::reverseSpan)(x.toInt(), y.toInt())
    },
    MOVE to { (_, x, y) ->
        (::move)(x.toInt(), y.toInt())
    }
)

fun parseReverse(str: String): Operation = str.match(
    SWAP_POSITION to { (_, x, y) ->
        (::swapPosition)(x.toInt(), y.toInt())
    },
    SWAP_LETTER to { (_, x, y) ->
        (::swapLetters)(x, y)
    },
    ROTATE to { (_, direction, steps) ->
        (::rotate)(if (direction == "left") "right" else "left", steps.toInt())
    },
    ROTATE_LETTER to { (_, x) ->
        (::reverseRotateLetter)(x)
    },
    REVERSE_SPAN to { (_, x, y) ->
        (::reverseSpan)(x.toInt(), y.toInt())
    },
    MOVE to { (_, x, y) ->
        (::move)(y.toInt(), x.toInt())
    }
)

fun rotateLetter(x: String, str: String): String {
    val index = str.indexOf(x[0])
    val rotations = 1 + index + if (index >= 4) 1 else 0
    return rotate("right", rotations, str)
}

fun reverseRotateLetter(x: String, str: String): String {
    val originalIndex = when (str.indexOf(x[0])) {
        1 -> 0; 3 -> 1; 5 -> 2; 7 -> 3; 2 -> 4; 4 -> 5; 6 -> 6; 0 -> 7
        else -> throw IllegalArgumentException()
    }
    val rotations = 1 + originalIndex + if (originalIndex >= 4) 1 else 0
    return rotate("left", rotations, str)
}

fun move(x: Int, y: Int, str: String): String {
    val start = str[0 until Math.min(x, y)]
    val end = str[Math.max(x, y) + 1 until str.length]
    val oldMiddle = str[Math.min(x, y)..Math.max(x, y)]
    val middle = when {
        x < y -> rotate("left", 1, oldMiddle).dropLast(1) + oldMiddle.first()
        x > y -> oldMiddle.last() + rotate("right", 1, oldMiddle).drop(1)
        else -> oldMiddle
    }
    return start + middle + end
}

fun swapPosition(x: Int, y: Int, str: String): String {
    val first = Math.min(x, y)
    val second = Math.max(x, y)
    return str.take(first) +
        str[second] +
        str.drop(first + 1).take(second - first - 1) +
        str[first] +
        str.drop(second + 1)
}

fun swapLetters(x: String, y: String, str: String) = str.map { when (it) {
    x[0] -> y[0]
    y[0] -> x[0]
    else -> it
} }.joinToString("")

fun reverseSpan(x: Int, y: Int, str: String) =
    str[0 until x] + str[x..y].reversed() + str[y+1 until str.length]

fun rotate(direction: String, steps: Int, str: String) = when (direction) {
    "right" -> (1..steps).fold(str) { s, _ -> s.last() + s.dropLast(1) }
    "left" -> (1..steps).fold(str) { s, _ -> s.drop(1) + s.first() }
    else -> throw IllegalArgumentException()
}

private operator fun String.get(range: IntRange) = this.substring(range)


val input = """
rotate based on position of letter d
move position 1 to position 6
swap position 3 with position 6
rotate based on position of letter c
swap position 0 with position 1
rotate right 5 steps
rotate left 3 steps
rotate based on position of letter b
swap position 0 with position 2
rotate based on position of letter g
rotate left 0 steps
reverse positions 0 through 3
rotate based on position of letter a
rotate based on position of letter h
rotate based on position of letter a
rotate based on position of letter g
rotate left 5 steps
move position 3 to position 7
rotate right 5 steps
rotate based on position of letter f
rotate right 7 steps
rotate based on position of letter a
rotate right 6 steps
rotate based on position of letter a
swap letter c with letter f
reverse positions 2 through 6
rotate left 1 step
reverse positions 3 through 5
rotate based on position of letter f
swap position 6 with position 5
swap letter h with letter e
move position 1 to position 3
swap letter c with letter h
reverse positions 4 through 7
swap letter f with letter h
rotate based on position of letter f
rotate based on position of letter g
reverse positions 3 through 4
rotate left 7 steps
swap letter h with letter a
rotate based on position of letter e
rotate based on position of letter f
rotate based on position of letter g
move position 5 to position 0
rotate based on position of letter c
reverse positions 3 through 6
rotate right 4 steps
move position 1 to position 2
reverse positions 3 through 6
swap letter g with letter a
rotate based on position of letter d
rotate based on position of letter a
swap position 0 with position 7
rotate left 7 steps
rotate right 2 steps
rotate right 6 steps
rotate based on position of letter b
rotate right 2 steps
swap position 7 with position 4
rotate left 4 steps
rotate left 3 steps
swap position 2 with position 7
move position 5 to position 4
rotate right 3 steps
rotate based on position of letter g
move position 1 to position 2
swap position 7 with position 0
move position 4 to position 6
move position 3 to position 0
rotate based on position of letter f
swap letter g with letter d
swap position 1 with position 5
reverse positions 0 through 2
swap position 7 with position 3
rotate based on position of letter g
swap letter c with letter a
rotate based on position of letter g
reverse positions 3 through 5
move position 6 to position 3
swap letter b with letter e
reverse positions 5 through 6
move position 6 to position 7
swap letter a with letter e
swap position 6 with position 2
move position 4 to position 5
rotate left 5 steps
swap letter a with letter d
swap letter e with letter g
swap position 3 with position 7
reverse positions 0 through 5
swap position 5 with position 7
swap position 1 with position 7
swap position 1 with position 7
rotate right 7 steps
swap letter f with letter a
reverse positions 0 through 7
rotate based on position of letter d
reverse positions 2 through 4
swap position 7 with position 1
swap letter a with letter h
""".trimIndent()