package adventofcode

import kotlin.math.abs

data class Grid2D<T> private constructor(
    val map: DefaultedMap<Coord2D, T>
) : DefaultedMap<Coord2D, T> by map {
    constructor(
        map: Map<Coord2D, T>,
        default: T
    ) : this(map.toMutableMap().withDefault(default))

    fun xRange() = keys.map { it.x }.let { it.min()!!..it.max()!! }
    fun yRange() = keys.map { it.y }.let { it.min()!!..it.max()!! }
}

fun <T> Grid2D<T>.center(): Coord2D {
    val xs = xRange().count()
    val ys = yRange().count()
    fun Int.isEven() = this % 2 == 0
    if (xs.isEven() || ys.isEven()) {
        throw IllegalArgumentException()
    }
    return Coord2D(xs / 2, ys / 2)
}

fun <T> Grid2D<T>.print(mark: Coord2D? = null) {
    yRange().reversed().map { y -> xRange().map { x ->
        when {
            y == mark?.y && x == mark.x     -> "["
            y == mark?.y && x == mark.x + 1 -> "]"
            else -> " "
        } + this[Coord2D(x, y)].toString()
    } }.forEach { line ->
        println(line.joinToString(""))
    }
}

fun <T> Iterable<Iterable<T>>.toGrid(default: T): Grid2D<T> = this
    .mapIndexed { y, row ->
        row.mapIndexed { x, value -> Coord2D(x = x, y = y) to value }
    }
    .flatMap { it }
    .associate { it }
    .let { Grid2D(it, default) }

fun <T> String.toGrid(default: T, map: (Char) -> T): Grid2D<T> = this
    .lines()
    .reversed()
    .map { line -> line.map(map) }
    .toGrid(default)

fun c(x: Int, y: Int) = Coord2D(x, y)
data class Coord2D(val x: Int, val y: Int)

val Coord2D.neighbours get() = setOf(right(), left(), up(), down())

fun Coord2D.right() = this.copy(x = this.x + 1)
fun Coord2D.left() = this.copy(x = this.x - 1)
fun Coord2D.up() = this.copy(y = this.y + 1)
fun Coord2D.down() = this.copy(y = this.y - 1)

fun Coord2D.move(direction: Direction) = direction.move(this)

fun manhattanDistance(c1: Coord2D, c2: Coord2D) = abs(c1.x - c2.x) + abs(c1.y - c2.y)

enum class Direction(val move: (Coord2D) -> Coord2D) {
    UP(Coord2D::up),
    DOWN(Coord2D::down),
    LEFT(Coord2D::left),
    RIGHT(Coord2D::right);

    fun turnLeft() = when (this) {
        Direction.UP -> Direction.LEFT
        Direction.DOWN -> Direction.RIGHT
        Direction.LEFT -> Direction.DOWN
        Direction.RIGHT -> Direction.UP
    }
    fun turnRight() = when (this) {
        Direction.UP -> Direction.RIGHT
        Direction.DOWN -> Direction.LEFT
        Direction.LEFT -> Direction.UP
        Direction.RIGHT -> Direction.DOWN
    }
}