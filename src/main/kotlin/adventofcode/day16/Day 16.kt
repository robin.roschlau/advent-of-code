package adventofcode.day16

import kotlin.coroutines.experimental.buildSequence

val input = "11100010111110100"

fun main(args: Array<String>) {
    println(checksum(stretchToLength(input, 272)))
    println(checksum(stretchToLength(input, 35651584)))
}

fun stretchToLength(a: String, length: Int): String {
    var b = a
    while (b.length < length) {
        b = stretch(b)
    }
    return b.take(length)
}

fun stretch(a: String): String {
    var b = a
    b = b.reversed()
    b = b.map { when (it) {
        '0' -> '1'
        '1' -> '0'
        else -> throw IllegalArgumentException()
    }}.joinToString("")
    return a + '0' + b
}

tailrec fun checksum(input: String): String {
    val checksum = input.pairs()
        .map { if (it.first == it.second) '1' else '0' }
        .joinToString("")
    if (checksum.length % 2 != 0) {
        return checksum
    }
    return checksum(checksum)
}

fun String.pairs() = buildSequence {
    val iter = this@pairs.iterator()
    while (iter.hasNext()) {
        yield(iter.next() to iter.next())
    }
}