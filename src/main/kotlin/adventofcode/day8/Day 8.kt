package adventofcode.day8

class Display(val height: Int, val width: Int) {
    val d = Array<BooleanArray>(height) { BooleanArray(width) { false } }

    operator fun get(row: Int) = d[row]

    fun print() {
        for (row in this.d) {
            for (pixel in row) {
                print(if(pixel) '#' else ' ')
            }
            println()
        }
        println()
    }

    fun count() = d.flatMap { it.toList() }.count { it == true }
}

fun main(args: Array<String>) {
    val display = Display(height = 6, width = 50)
    input.lines()
        .map { parse(it) }
        .forEach {
            display.print()
            it.executeOn(display)
        }
    display.print()
    println(display.count())

}

val RECT_PATTERN = Regex("rect (\\d+)x(\\d+)")
val ROT_COL_PATTERN = Regex("rotate column x=(\\d+) by (\\d+)")
val ROT_ROW_PATTERN = Regex("rotate row y=(\\d+) by (\\d+)")

fun parse(line: String): DisplayCommand {
    RECT_PATTERN.matchEntire(line)?.let { match ->
        return Rect(match.groupValues[1].toInt(), match.groupValues[2].toInt())
    }
    ROT_COL_PATTERN.matchEntire(line)?.let { match ->
        return RotateCol(match.groupValues[1].toInt(), match.groupValues[2].toInt())
    }
    ROT_ROW_PATTERN.matchEntire(line)?.let { match ->
        return RotateRow(match.groupValues[1].toInt(), match.groupValues[2].toInt())
    }
    throw IllegalArgumentException()
}

sealed class DisplayCommand {
    abstract fun executeOn(display: Display)
}
class Rect(val width: Int, val height: Int) : DisplayCommand() {
    override fun executeOn(display: Display) {
        for (y in 0 until height) {
            for (x in 0 until width) {
                display[y][x] = true
            }
        }
    }

}
class RotateCol(val col: Int, val count: Int) : DisplayCommand() {
    override fun executeOn(display: Display) {
        val colCopy = (0 until display.height).map { display[it][col] }
        for (row in 0 until display.height) {
            val targetRow = (row + count) % display.height
            display[targetRow][col] = colCopy[row]
        }
    }
}

class RotateRow(val row: Int, val count: Int) : DisplayCommand() {
    override fun executeOn(display: Display) {
        val rowCopy = (0 until display.width).map { display[row][it] }
        for (col in 0 until display.width) {
            val targetCol = (col + count) % display.width
            display[row][targetCol] = rowCopy[col]
        }
    }
}