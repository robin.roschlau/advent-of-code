package adventofcode.day4


fun main(args: Array<String>) {
    input.lines()
        .map { Room.from(it) }
        .filter { it.isReal }
        .onEach { println("${it.decryptedName}: ${it.sectorId}") }
}

data class Room(val name: String, val sectorId: Int, val checksum: String) {

    val isReal: Boolean
        get() = checksum == checkSumFor(name)

    val decryptedName: String
        get() = name.map {
            when (it) {
                '-' -> ' '
                else -> it.shift(sectorId)
            }
        }.joinToString(separator = "")

    private fun checkSumFor(name: String): String =
        name.replace("-", "")
            .groupBy { it }
            .map { CharacterCount(it.key, it.value.size) }
            .groupBy { it.count }
            .entries
            .sortedByDescending { it.key }
            .flatMap { entry -> entry.value.sortedBy { it.char } }
            .take(5)
            .map { it.char }
            .joinToString(separator = "")

    companion object {
        fun from(str: String): Room {
            val (name, sectorId, checksum) = Regex("((?:[a-z]+-)+)(\\d+)\\[(.+)]").matchEntire(str)!!.destructured
            return Room(name.dropLast(1), sectorId.toInt(), checksum)
        }
    }

    override fun toString(): String {
        return "${checkSumFor(name)}"
    }
}

private tailrec fun Char.shift(times: Int): Char = when {
    times < 1 -> this
    else -> this.shift().shift(times - 1)
}
private inline fun Char.shift(): Char {
    if (this == 'z') return 'a'
    return this + 1
}

data class CharacterCount(val char: Char, val count: Int)