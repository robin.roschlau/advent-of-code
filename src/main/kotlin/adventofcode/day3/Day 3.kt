package adventofcode.day3

fun main(args: Array<String>) {
    val raw = input.lines()
        .filterNot { it.isBlank() }
        .map { it.split(Regex("\\W")).filterNot { it.isBlank() }.map { it.toInt() } }
    val first = raw.map { it[0] }
    val second = raw.map { it[1] }
    val third = raw.map { it[2] }
    val all = first + second + third
    val res = all.asSequence().chunked(3)
        .map { it.sortedDescending() }
        .onEach { println("$it -> sum = ${it[1] + it[2]}") }
        .count { it[0] < it[1] + it[2] }
    println(res)
}