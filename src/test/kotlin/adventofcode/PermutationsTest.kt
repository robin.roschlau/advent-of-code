package adventofcode

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class VariationsTest {

    @Test(expected = IllegalArgumentException::class)
    fun `fails on k bigger than size and no repetitions`() {
        setOf(0).variations(k = 2)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `fails on k smaller than 0`() {
        setOf(0).variations(k = -1, repetitions = false)
    }

    @Test
    fun `test set variations, n = 1`() {
        val input = setOf(0)
        assertThat(input.variations(1), equalTo(setOf(
            listOf(0)
        )))
        assertThat(input.variations(1, repetitions = true), equalTo(setOf(
            listOf(0)
        )))
        assertThat(input.variations(0), equalTo(setOf()))
    }

    @Test
    fun `test set variations, n = 2, rep = true`() {
        val input = setOf(0, 1)
        assertThat(input.variations(3, repetitions = true), equalTo(setOf(
            listOf(0, 0, 0),
            listOf(0, 0, 1),
            listOf(0, 1, 0),
            listOf(0, 1, 1),
            listOf(1, 0, 0),
            listOf(1, 0, 1),
            listOf(1, 1, 0),
            listOf(1, 1, 1)
        )))
        assertThat(input.variations(2, repetitions = true), equalTo(setOf(
            listOf(0, 0),
            listOf(0, 1),
            listOf(1, 0),
            listOf(1, 1)
        )))
        assertThat(input.variations(1, repetitions = true), equalTo(setOf(
            listOf(0),
            listOf(1)
        )))
    }

    @Test
    fun `test set variations, n = 3`() {
        val input = setOf(0, 1, 2)
        assertThat(input.variations(3), equalTo(setOf(
            listOf(0, 1, 2),
            listOf(0, 2, 1),
            listOf(1, 2, 0),
            listOf(1, 0, 2),
            listOf(2, 1, 0),
            listOf(2, 0, 1)
        )))
        assertThat(input.variations(2), equalTo(setOf(
            listOf(0, 1),
            listOf(0, 2),
            listOf(1, 0),
            listOf(1, 2),
            listOf(2, 0),
            listOf(2, 1)
        )))
        assertThat(input.variations(1), equalTo(setOf(
            listOf(0),
            listOf(1),
            listOf(2)
        )))
    }

    @Test
    fun `test set variations, n = 3, rep = true`() {
        val input = setOf(0, 1, 2)
        assertThat(input.variations(3, repetitions = true), equalTo(setOf(
            listOf(0, 0, 0),
            listOf(0, 0, 1),
            listOf(0, 0, 2),
            listOf(0, 1, 0),
            listOf(0, 1, 1),
            listOf(0, 1, 2),
            listOf(0, 2, 0),
            listOf(0, 2, 1),
            listOf(0, 2, 2),
            listOf(1, 0, 0),
            listOf(1, 0, 1),
            listOf(1, 0, 2),
            listOf(1, 1, 0),
            listOf(1, 1, 1),
            listOf(1, 1, 2),
            listOf(1, 2, 0),
            listOf(1, 2, 1),
            listOf(1, 2, 2),
            listOf(2, 0, 0),
            listOf(2, 0, 1),
            listOf(2, 0, 2),
            listOf(2, 1, 0),
            listOf(2, 1, 1),
            listOf(2, 1, 2),
            listOf(2, 2, 0),
            listOf(2, 2, 1),
            listOf(2, 2, 2)
        )))
        assertThat(input.variations(2, repetitions = true), equalTo(setOf(
            listOf(0, 0),
            listOf(0, 1),
            listOf(0, 2),
            listOf(1, 0),
            listOf(1, 1),
            listOf(1, 2),
            listOf(2, 0),
            listOf(2, 1),
            listOf(2, 2)
        )))
        assertThat(input.variations(1, repetitions = true), equalTo(setOf(
            listOf(0),
            listOf(1),
            listOf(2)
        )))
    }

}