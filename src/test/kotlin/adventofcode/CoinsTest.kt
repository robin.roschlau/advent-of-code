package adventofcode

import org.junit.Test

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat

class CoinsTest {

    @Test
    fun test() {
        assertCombination(1, mapOf(Coin.PENNY to 1))
        assertCombination(2, mapOf(Coin.PENNY to 2))
        assertCombination(3, mapOf(Coin.PENNY to 3))
        assertCombination(4, mapOf(Coin.PENNY to 4))
        assertCombination(5, mapOf(Coin.NICKEL to 1))
        assertCombination(7, mapOf(Coin.NICKEL to 1, Coin.PENNY to 2))
        assertCombination(26, mapOf(Coin.QUARTER to 1, Coin.PENNY to 1))
    }

    private fun assertCombination(target: Int, combination: Map<Coin, Int>) {
        assertThat(findSmallestCombination(target), equalTo(combination))
    }

}