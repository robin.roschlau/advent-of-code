package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day16Test {

    @Test
    fun `part 2 test cases`() {
        val result = Day16.part2(Day16.parse("s1,x3/4,pe/b"), "abcde", 2)
        assertThat(result, equalTo("ceadb"))
    }
}