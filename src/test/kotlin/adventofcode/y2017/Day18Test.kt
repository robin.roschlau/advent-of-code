package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day18Test {

    @Test
    fun `part 1 test cases`() {
        val input = """
            set a 1
            add a 2
            mul a a
            mod a 5
            snd a
            set a 0
            rcv a
            jgz a -1
            set a 1
            jgz a -2
        """.trimIndent()
        assertThat(Day18.part1(Day18.parse(input)), equalTo(4L))
    }

    @Test
    fun `part 2 test cases`() {val input = """
            snd 1
            snd 2
            snd p
            rcv a
            rcv b
            rcv c
            rcv d
        """.trimIndent()
        assertThat(Day18.part2(Day18.parse(input)), equalTo(3L))
    }
}