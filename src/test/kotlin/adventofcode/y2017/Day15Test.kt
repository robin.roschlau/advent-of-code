package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day15Test {

    @Test
    fun `generator works`() {
        val result = Day15.generator(65, 16807).take(5).toList()
        assertThat(result, contains(1092455, 1181022009, 245556042, 1744312007, 1352636452))
        val result2 = Day15.generator(8921, 48271).take(5).toList()
        assertThat(result2, contains(430625591, 1233683848, 1431495498, 137874439, 285222916))
    }

    @Test
    fun `part 1 test case`() {
        val genA = sequenceOf(1092455, 1181022009, 245556042, 1744312007, 1352636452)
        val genB = sequenceOf(430625591, 1233683848, 1431495498, 137874439, 285222916)
        assertThat(Day15.part1(genA, genB, 5), equalTo(1))
    }

    @Test
    fun `part 2 test case`() {
        val genA = Day15.generator(65, 16807)
        val genB = Day15.generator(8921, 48271)
        assertThat(Day15.part2(genA, genB, 1057), equalTo(1))
    }
}