package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day25Test {

    @Test
    fun `part 1 test case`() {
        val input = """
            Begin in state A.
            Perform a diagnostic checksum after 6 steps.

            In state A:
              If the current value is 0:
                - Write the value 1.
                - Move one slot to the right.
                - Continue with state B.
              If the current value is 1:
                - Write the value 0.
                - Move one slot to the left.
                - Continue with state B.

            In state B:
              If the current value is 0:
                - Write the value 1.
                - Move one slot to the left.
                - Continue with state A.
              If the current value is 1:
                - Write the value 1.
                - Move one slot to the right.
                - Continue with state A.
        """.trimIndent()
        assertThat(Day25.part1(Day25.parse(input)), equalTo(3))
    }
}