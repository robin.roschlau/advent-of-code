package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day20Test {

    @Test
    fun `part 1 test cases`() {
        val input = """
            p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>
            p=< 4,0,0>, v=< 0,0,0>, a=<-2,0,0>
        """.trimIndent().lines()
        assertThat(Day20.part1(Day20.parse(input)), equalTo(0))
    }

    @Test
    fun `part 2 test cases`() {
        val input = """
            p=<-6,0,0>, v=< 3,0,0>, a=< 0,0,0>
            p=<-4,0,0>, v=< 2,0,0>, a=< 0,0,0>
            p=<-2,0,0>, v=< 1,0,0>, a=< 0,0,0>
            p=< 3,0,0>, v=<-1,0,0>, a=< 0,0,0>
        """.trimIndent().lines()
        assertThat(Day20.part2(Day20.parse(input)), equalTo(1))
    }
}