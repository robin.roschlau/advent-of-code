package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasItems
import org.junit.Test

class Day21Test {

    @Test
    fun `test symmetries`() {
        val input = arrayOf(
            arrayOf(false, true, false),
            arrayOf(false, false, true),
            arrayOf(true, true, true)
        )
        assertThat(Day21.Picture(input).symmetries.map { it.pixels }, hasItems(
            arrayOf(
                arrayOf(false, true, false),
                arrayOf(false, false, true),
                arrayOf(true, true, true)
            ),
            arrayOf(
                arrayOf(false, true, false),
                arrayOf(true, false, false),
                arrayOf(true, true, true)
            ),
            arrayOf(
                arrayOf(true, false, false),
                arrayOf(true, false, true),
                arrayOf(true, true, false)
            ),
            arrayOf(
                arrayOf(true, true, true),
                arrayOf(false, false, true),
                arrayOf(false, true, false)
            )
        ))
    }

    @Test
    fun `part 1 test case`() {
        val input = """
            ../.# => ##./#../...
            .#./..#/### => #..#/..../..../#..#
        """.trimIndent().lines()
        assertThat(Day21.part1(Day21.parse(input), 2), equalTo(12))
    }
}