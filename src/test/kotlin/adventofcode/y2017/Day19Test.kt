package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day19Test {

    @Test
    fun `part 1 test cases`() {
        val input = """
        !    |
        !    |  +--+
        !    A  |  C
        !F---|----E|--+
        !    |  |  |  D
        !    +B-+  +--+
        """.trimMargin("!").lines()
        assertThat(Day19.part1and2(Day19.parse(input)), equalTo("ABCDEF\n38"))
    }
}