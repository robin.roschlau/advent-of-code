package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day24Test {

    @Test
    fun `part 1 test case`() {
        val input = """
            0/2
            2/2
            2/3
            3/4
            3/5
            0/1
            10/1
            9/10
        """.trimIndent().lines()
        assertThat(Day24.part1(Day24.parse(input)), equalTo(31))
    }

    @Test
    fun `part 2 test case`() {
        val input = """
            0/2
            2/2
            2/3
            3/4
            3/5
            0/1
            10/1
            9/10
        """.trimIndent().lines()
        assertThat(Day24.part2(Day24.parse(input)), equalTo(19))
    }
}