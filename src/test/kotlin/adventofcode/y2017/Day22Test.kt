package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day22Test {

    @Test
    fun `part 1 test case`() {
        val input = """
            ..#
            #..
            ...
        """.trimIndent()

        assertThat(Day22.part1(Day22.parse(input), 7, true), equalTo(5))
        assertThat(Day22.part1(Day22.parse(input), 70), equalTo(41))
        assertThat(Day22.part1(Day22.parse(input), 10000), equalTo(5587))
    }

    @Test
    fun `part 2 test case`() {
        val input = """
            ..#
            #..
            ...
        """.trimIndent()
        assertThat(Day22.part2(Day22.parse(input), 100), equalTo(26))
        assertThat(Day22.part2(Day22.parse(input), 10000000), equalTo(2511944))
    }
}