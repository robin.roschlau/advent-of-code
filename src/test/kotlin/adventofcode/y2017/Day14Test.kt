package adventofcode.y2017

import adventofcode.c
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day14Test {

    val testGrid = listOf(
        listOf(1,1,0,2,0,3,0,0),
        listOf(0,1,0,2,0,3,0,4),
        listOf(0,0,0,0,5,0,6,0),
        listOf(7,0,8,0,5,5,0,9),
        listOf(0,8,8,0,5,0,0,0),
        listOf(8,8,0,0,5,0,0,8),
        listOf(0,8,0,0,0,8,0,0),
        listOf(8,8,0,8,0,8,8,0)
    ).map { it.map { it != 0 } }

    @Test
    fun `part 1 test cases`() {
        assertThat(Day14.part1("flqrgnkx"), equalTo(8108))
    }

    @Test
    fun `find connected finds same set for all coords in a region`() {
        with(Day14) {
            val group = setOf(
                c(0, 0),
                c(1, 0),
                c(1, 1)
            )
            assertThat(testGrid.findRegions(c(0, 0)), equalTo(group))
            assertThat(testGrid.findRegions(c(1, 0)), equalTo(group))
            assertThat(testGrid.findRegions(c(1, 1)), equalTo(group))
        }
    }

    @Test
    fun `part 2 test cases`() {
        assertThat(Day14.part2("flqrgnkx"), equalTo(1242))
    }
}