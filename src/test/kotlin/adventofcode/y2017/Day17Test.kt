package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day17Test {

    @Test
    fun `part 1 test cases`() {
        assertThat(Day17.part1(3, 2017), equalTo(638))
    }

    @Test
    fun `part 2 test cases`() {
        assertThat(Day17.part2(3, 9), equalTo(9))
    }
}