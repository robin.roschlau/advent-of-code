package adventofcode.y2017

import adventofcode.c
import adventofcode.manhattanDistance
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day03Test {

    @Test
    fun `manhattan distance`() {
        assertThat(manhattanDistance(c(0, 0), c(0, 0)), equalTo(0))
        assertThat(manhattanDistance(c(0, 0), c(2, 1)), equalTo(3))
        assertThat(manhattanDistance(c(0, 0), c(0, 2)), equalTo(2))
    }

    @Test
    fun `coordinate conversion`() {
        assertThat(1.toCoord(), equalTo(c(0, 0)))
        assertThat(2.toCoord(), equalTo(c(1, 0)))
        assertThat(3.toCoord(), equalTo(c(1, 1)))
        assertThat(6.toCoord(), equalTo(c(-1, 0)))
        assertThat(9.toCoord(), equalTo(c(1, -1)))
        assertThat(21.toCoord(), equalTo(c(-2, -2)))
    }

    @Test
    fun `neighbours of center`() {
        assertThat(neighbours(c(0, 0)), containsInAnyOrder(
            c(0, 1),
            c(1, 1),
            c(1, 0),
            c(1, -1),
            c(0, -1),
            c(-1, -1),
            c(-1, 0),
            c(-1, 1)
        ))
    }
}