package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day05Test {

    @Test
    fun `findOut testcase`() {
        val input = mutableListOf(0, 3, 0, 1, -3)
        val steps = findOut(input)
        assertThat(steps, equalTo(5))
    }

    @Test
    fun `findOut testcase part 2`() {
        val input = mutableListOf(0, 3, 0, 1, -3)
        val steps = findOutPart2(input)
        assertThat(steps, equalTo(10))
    }

}