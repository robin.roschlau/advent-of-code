package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day01Test {

    // Part 1

    @Test
    fun `next char captcha test cases`() {
        assertThat(captchaSumNextChar("1122"), equalTo(3))
        assertThat(captchaSumNextChar("1111"), equalTo(4))
        assertThat(captchaSumNextChar("1234"), equalTo(0))
        assertThat(captchaSumNextChar("91212129"), equalTo(9))
    }

    // Part 2

    @Test
    fun `opposite index`() {
        assertThat("0".oppositeIndex(0), equalTo(0))
        assertThat("0123".oppositeIndex(0), equalTo(2))
        assertThat("0123".oppositeIndex(1), equalTo(3))
        assertThat("0123".oppositeIndex(2), equalTo(0))
        assertThat("0123".oppositeIndex(3), equalTo(1))
    }

    @Test
    fun `opposite char captcha test cases`() {
        assertThat(captchaSumOppositeChar("1212"), equalTo(6))
        assertThat(captchaSumOppositeChar("1221"), equalTo(0))
        assertThat(captchaSumOppositeChar("123425"), equalTo(4))
        assertThat(captchaSumOppositeChar("123123"), equalTo(12))
        assertThat(captchaSumOppositeChar("12131415"), equalTo(4))
    }

}