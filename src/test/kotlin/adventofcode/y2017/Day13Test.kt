package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day13Test {

    @Test
    fun `part 1 test case`() {
        val input = Day13.parse("""
            0: 3
            1: 2
            4: 4
            6: 4
        """.trimIndent())
        assertThat(Day13.part1(input), equalTo(24))
    }

    @Test
    fun `part 2 test cases`() {
        val input = Day13.parse("""
            0: 3
            1: 2
            4: 4
            6: 4
        """.trimIndent())
        assertThat(Day13.part2(input), equalTo(10))
    }
}