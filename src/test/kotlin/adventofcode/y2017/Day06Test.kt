package adventofcode.y2017

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day06Test {

    @Test
    fun `test reallocation cycle`() {
        val input = listOf(0, 2, 7, 0)
        var result = reallocationCycle(input)
        assertThat(result, equalTo(listOf(2, 4, 1, 2)))
        result = reallocationCycle(result)
        assertThat(result, equalTo(listOf(3, 1, 2, 3)))
        result = reallocationCycle(result)
        assertThat(result, equalTo(listOf(0, 2, 3, 4)))
        result = reallocationCycle(result)
        assertThat(result, equalTo(listOf(1, 3, 4, 1)))
        result = reallocationCycle(result)
        assertThat(result, equalTo(listOf(2, 4, 1, 2)))
    }
    @Test
    fun `test cases`() {
        val input = listOf(0, 2, 7, 0)
        var result = solve(input)
        assertThat(result.first, equalTo(5))
        assertThat(result.second, equalTo(4))
    }
}