package adventofcode.y2017

import adventofcode.y2017.Day11.Direction.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class Day11Test {

    @Test
    fun `part 1 test case 1`() {
        assertThat(Day11.part1(listOf(NE, NE, NE)), equalTo(3))
    }

    @Test
    fun `part 1 test case 2`() {
        assertThat(Day11.part1(listOf(NE, NE, SW, SW)), equalTo(0))
    }

    @Test
    fun `part 1 test case 3`() {
        assertThat(Day11.part1(listOf(NE, NE, S, S)), equalTo(2))
    }

    @Test
    fun `part 1 test case 4`() {
        assertThat(Day11.part1(listOf(SE, SW, SE, SW, SW)), equalTo(3))
    }
}