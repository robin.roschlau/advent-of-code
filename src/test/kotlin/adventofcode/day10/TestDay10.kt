package adventofcode.day10

import kotlinx.coroutines.experimental.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert
import org.junit.Assert.assertThat
import org.junit.Test

class TestDay10 {

    @Test
    fun test() = runBlocking {
        assertThat(
            determineBot(
                simulate("""
                    value 5 goes to bot 2
                    bot 2 gives low to bot 1 and high to bot 0
                    value 3 goes to bot 1
                    bot 1 gives low to output 1 and high to bot 0
                    bot 0 gives low to output 2 and high to output 0
                    value 2 goes to bot 2
                """.trimIndent()).botLog,
                5, 2
            ),
            equalTo(2)
        )
    }

}