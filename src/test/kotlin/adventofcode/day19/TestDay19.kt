package adventofcode.day19

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TestDay19 {

    @Test
    fun `with 5 elves, elf 3 gets all the presents`() {
        assertThat(getsAllPresents(5), equalTo(3))
    }

    @Test
    fun `with 5 elves, elf 2 gets all the presents`() {
        assertThat(partTwo(5), equalTo(2))
    }

}