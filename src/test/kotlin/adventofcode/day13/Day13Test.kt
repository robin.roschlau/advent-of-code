package adventofcode.day13

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class Day13Test {

    @Test
    fun `spaces are calculated correctly`() {
        assertThat(getSpace(0, 0, 10), equalTo(Space.OPEN))
        assertThat(getSpace(0, 2, 10), equalTo(Space.WALL))
        assertThat(getSpace(2, 0, 10), equalTo(Space.OPEN))
        assertThat(getSpace(2, 5, 10), equalTo(Space.WALL))
        assertThat(getSpace(9, 6, 10), equalTo(Space.WALL))
    }

    @Test
    fun `shortest path is calculated correctly`() {
        assertThat(shortestPath(10, Node(1, 1), Node(7, 4)).size, equalTo(12))
    }

    @Test
    fun `reachable is calculated correctly`() {
        assertThat(reachable(10, Node(1, 1), 3).size, equalTo(6))
    }

}