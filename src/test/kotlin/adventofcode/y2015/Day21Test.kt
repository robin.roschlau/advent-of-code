package adventofcode.y2015

import org.junit.Assert.assertTrue
import org.junit.Test

class Day21Test {

    @Test
    fun `test winning predicate`() {
        with(Day21) {
            val player = Day21.Equipment("", cost = 0, hitpoints = 8, damage = 5, armor = 5)
            val boss = Day21.Boss(hitpoints = 12, damage = 7, armor = 2)
            assertTrue(player.winsAgainst(boss))
        }
    }

}