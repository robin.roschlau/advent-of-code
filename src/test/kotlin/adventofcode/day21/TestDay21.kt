package adventofcode.day21

import org.hamcrest.Matchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TestDay21 {

    @Test
    fun `test swap position`() {
        val input = "abcde"
        val op = "swap position 4 with position 0"
        val result = parse(op)(input)
        assertThat(result, equalTo("ebcda"))
    }

    @Test
    fun `test swap letters`() {
        val input = "ebcda"
        val op = "swap letter d with letter b"
        val result = parse(op)(input)
        assertThat(result, equalTo("edcba"))
    }

    @Test
    fun `reverse span`() {
        val input = "edcba"
        val op = "reverse positions 0 through 4"
        val result = parse(op)(input)
        assertThat(result, equalTo("abcde"))
    }

    @Test
    fun `rotate`() {
        val input = "abcde"
        val op = "rotate left 1 step"
        val result = parse(op)(input)
        assertThat(result, equalTo("bcdea"))
    }

    @Test
    fun `move forwards`() {
        val input = "bcdea"
        val op = "move position 1 to position 4"
        val result = parse(op)(input)
        assertThat(result, equalTo("bdeac"))
    }

    @Test
    fun `move backwards`() {
        val input = "bdeac"
        val op = "move position 3 to position 0"
        val result = parse(op)(input)
        assertThat(result, equalTo("abdec"))
    }

    @Test
    fun `rotate letter 2`() {
        val input = "abdec"
        val op = "rotate based on position of letter b"
        val result = parse(op)(input)
        assertThat(result, equalTo("ecabd"))
    }

    @Test
    fun `rotate letter 4`() {
        val input = "ecabd"
        val op = "rotate based on position of letter d"
        val result = parse(op)(input)
        assertThat(result, equalTo("decab"))
    }

    @Test
    fun `test complete`() {
        val input = "abcde"
        val ops = """
            swap position 4 with position 0
            swap letter d with letter b
            reverse positions 0 through 4
            rotate left 1 step
            move position 1 to position 4
            move position 3 to position 0
            rotate based on position of letter b
            rotate based on position of letter d
        """.trimIndent()
        val result = ops.lines()
            .map { parse(it) }
            .fold(input) { pwd, operation -> operation(pwd) }
        assertThat(result, equalTo("decab"))
    }

    @Test
    fun `test reverse complete`() {
        val input = "decab"
        val ops = """
            swap position 4 with position 0
            swap letter d with letter b
            reverse positions 0 through 4
            rotate left 1 step
            move position 1 to position 4
            move position 3 to position 0
            rotate based on position of letter b
            rotate based on position of letter d
        """.trimIndent()
        val result = ops.lines()
            .reversed()
            .map { parseReverse(it) }
            .fold(input) { pwd, operation -> operation(pwd) }
        assertThat(result, equalTo("abcde"))
    }

}