package adventofcode.day20

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Test

class TestDay20 {

    @Test
    fun `test case 1`() {
        val input = """
            5-8
            0-2
            4-7
        """.trimIndent()
        val lowestUnblocked = nextUnblocked(input)
        assertThat(lowestUnblocked, equalTo(3L))
    }

    @Test
    fun `test case 2`() {
        val input = """
            5-8
            0-2
            4-7
        """.trimIndent()
        val allUnblocked = allUnblocked(input, max = 9)
        assertThat(allUnblocked, contains(3L, 9L))
    }

}