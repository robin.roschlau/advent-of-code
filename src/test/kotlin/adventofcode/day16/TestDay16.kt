package adventofcode.day16

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TestDay16 {

    @Test
    fun `test stretching`() {
        assertThat(stretch("1"), equalTo("100"))
        assertThat(stretch("0"), equalTo("001"))
        assertThat(stretch("11111"), equalTo("11111000000"))
        assertThat(stretch("111100001010"), equalTo("1111000010100101011110000"))
    }

    @Test
    fun `test stretching to length`() {
        assertThat(stretchToLength("10000", 20), equalTo("10000011110010000111"))
    }

    @Test
    fun `test checksum`() {
        assertThat(checksum("10000011110010000111"), equalTo("01100"))
    }

}