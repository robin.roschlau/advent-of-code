package adventofcode.day18

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TestDay18 {

    @Test
    fun `parsing works`() {
        val input = "^^..^"
        val result = parseRow(input)
        assertThat(result, equalTo(listOf(Tile.TRAP, Tile.TRAP, Tile.SAFE, Tile.SAFE, Tile.TRAP)))
    }

    @Test
    fun `printing works`() {
        val input = listOf(Tile.TRAP, Tile.TRAP, Tile.SAFE, Tile.SAFE, Tile.TRAP)
        val result = input.print()
        assertThat(result, equalTo("^^..^"))
    }

    @Test
    fun `nextRow works`() {
        val input = "..^^."
        val row1 = nextRow(parseRow(input))
        assertThat(row1.print(), equalTo(".^^^^"))
        val row2 = nextRow(row1)
        assertThat(row2.print(), equalTo("^^..^"))
    }

    @Test
    fun `counting safe tiles works`() {
        val input = ".^^.^.^^^^"
        val rows = rows(parseRow(input), 10)
        val result = countSafeTiles(rows)
        assertThat(result, equalTo(38))
    }

}

private fun Row.print() = this.joinToString("") { if (it == Tile.TRAP) "^" else "." }
