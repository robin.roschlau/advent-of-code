package adventofcode

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.contains
import org.junit.Test

class CollectionsHelpersTest {

    @Test
    fun `test circular list reversal`() {
        val list = listOf(0, 1, 2, 3, 4, 5)
        val result = list.reverse(0, 6)
        assertThat(result, contains(5, 4, 3, 2, 1, 0))
        assertThat(list.reverse(1, 4), contains(0, 4, 3, 2, 1, 5))
        assertThat(list.reverse(3, 5), contains(4, 3, 2, 1, 0, 5))
    }

}