package adventofcode.day17

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TestDay17 {

    @Test
    fun `test shortest path`() {
        assertThat(shortestPath("ihgpwlah"), equalTo("DDRRRD"))
        assertThat(shortestPath("kglvqrro"), equalTo("DDUDRLRRUDRD"))
        assertThat(shortestPath("ulqzkmiv"), equalTo("DRURDRUDDLLDLUURRDULRLDUUDDDRR"))
    }

    @Test
    fun `test longest path`() {
        assertThat(longestPath("ihgpwlah").length, equalTo(370))
        assertThat(longestPath("kglvqrro").length, equalTo(492))
        assertThat(longestPath("ulqzkmiv").length, equalTo(830))
    }

}