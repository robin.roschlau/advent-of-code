package adventofcode.day10

import adventofcode.day11.getMinimumNumberOfSteps
import adventofcode.day11.parseRoomState
import adventofcode.day11.testInput
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class TestDay11 {

    @Test
    fun test() {
        assertThat(
            getMinimumNumberOfSteps(parseRoomState(testInput)),
            equalTo(11)
        )
    }

}