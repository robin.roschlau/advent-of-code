package adventofcode

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class BinaryTreeTest {

    @Test
    fun `test list to tree`() {
        val list = listOf(0, 1, 2, 3, 4, 5, 6)
        val tree = list.toBinaryTree()
        assertThat(tree.size, equalTo(7))
        assertThat(tree, equalTo(
            BinaryTree(3,
                BinaryTree(1,
                    BinaryTree(0),
                    BinaryTree(2)
                ),
                BinaryTree(5,
                    BinaryTree(4),
                    BinaryTree(6)
                )
            )
        ))
    }

    @Test
    fun `test invert`() {
        assertThat(
            listOf(0, 1, 2, 3, 4, 5, 6).toBinaryTree().invert(),
            equalTo(listOf(6, 5, 4, 3, 2, 1, 0).toBinaryTree())
        )
    }

}