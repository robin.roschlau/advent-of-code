package adventofcode.day14

import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.*
import org.junit.Test

class TestDay14 {

    val testHash = "abc"

    @Test
    fun `hash works correctly`() {
        assertThat(simpleHash(testHash, 18).hash, containsString("cc38887a5"))
        assertThat(simpleHash(testHash, 39).hash, containsString("eee"))
        assertThat(simpleHash(testHash, 92).hash, containsString("999"))
        assertThat(simpleHash(testHash, 200).hash, containsString("99999"))
        assertThat(simpleHash(testHash, 816).hash, containsString("eeeee"))
    }

    @Test
    fun `determines keys correctly`() {
        assertNull(tryKey(testHash, 18, simpleHash))
        assertNotNull(tryKey(testHash, 39, simpleHash))
        assertNotNull(tryKey(testHash, 92, simpleHash))
        assertNotNull(tryKey(testHash, 22728, simpleHash))
    }

    @Test
    fun `finds nth key`() {
        assertThat(nthKey(testHash, 1, simpleHash).hash.index, equalTo(39L))
        assertThat(nthKey(testHash, 2, simpleHash).hash.index, equalTo(92L))
        assertThat(nthKey(testHash, 64, simpleHash).hash.index, equalTo(22728L))
    }

    @Test
    fun `stretches hashes correctly`() {
        assertThat(stretchedHash(testHash, 0, 2016).hash, equalTo("a107ff634856bb300138cac6568c0f24"))
    }

}