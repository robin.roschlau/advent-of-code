package adventofcode.day15

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class TestDay15 {

    @Test
    fun `test parser`() {
        assertThat(parseDisc("Disc #1 has 13 positions; at time=0, it is at position 10."),
            equalTo(Disc(1, 13, 10)))
        assertThat(parseDisc("Disc #5 has 5 positions; at time=0, it is at position 0."),
            equalTo(Disc(5, 5, 0)))
    }

    @Test
    fun `test positionAt`() {
        assertThat(Disc(0, 1, 0).positionAt(0), equalTo(0))
        assertThat(Disc(0, 1, 0).positionAt(1), equalTo(0))
        assertThat(Disc(0, 1, 0).positionAt(2), equalTo(0))
        assertThat(Disc(0, 5, 4).positionAt(1), equalTo(0))
        assertThat(Disc(0, 2, 1).positionAt(2), equalTo(1))
        assertThat(Disc(0, 5, 4).positionAt(6), equalTo(0))
        assertThat(Disc(0, 2, 1).positionAt(7), equalTo(0))
    }

    @Test
    fun `test testTime`() {
        val discs = listOf(Disc(1, 5, 4), Disc(2, 2, 1))
        assertFalse(testTime(0, discs))
        assertTrue(testTime(5, discs))
    }

}