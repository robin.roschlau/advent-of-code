package adventofcode.day9

import org.junit.Test

class DecompressionV1Test {

    @Test
    fun `decompress to raw string`() {
        assert(decompressV1("ADVENT") == "ADVENT")
    }

    @Test
    fun `case1`() {
        assert(decompressV1("A(1x5)BC") == "ABBBBBC")
    }

    @Test
    fun `case2`() {
        assert(decompressV1("(3x3)XYZ") == "XYZXYZXYZ")
    }

    @Test
    fun `case3`() {
        assert(decompressV1("A(2x2)BCD(2x2)EFG") == "ABCBCDEFEFG")
    }

    @Test
    fun `case4`() {
        assert(decompressV1("(6x1)(1x3)A") == "(1x3)A")
    }

    @Test
    fun `case5`() {
        assert(decompressV1("X(8x2)(3x3)ABCY") == "X(3x3)ABC(3x3)ABCY")
    }

}