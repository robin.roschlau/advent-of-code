package adventofcode.day9

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert
import org.junit.Assert.assertThat
import org.junit.Test

class DecompressionV2Test {

    @Test
    fun `decompress to raw string`() {
        assertThat(countV2("ADVENT"), equalTo(6L))
    }

    @Test
    fun `case1`() {
        assertThat(countV2(""), equalTo(0L))
    }

    @Test
    fun `case2`() {
        assertThat(countV2("(3x3)XYZ"), equalTo("XYZXYZXYZ".length.toLong()))
    }

    @Test
    fun `case3`() {
        assertThat(countV2("X(8x2)(3x3)ABCY"), equalTo("XABCABCABCABCABCABCY".length.toLong()))
    }

    @Test
    fun `case4`() {
        assertThat(countV2("(27x12)(20x12)(13x14)(7x10)(1x12)A"), equalTo(241920L))
    }

    @Test
    fun `case5`() {
        assertThat(countV2("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"), equalTo(445L))
    }

}