package adventofcode

import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.Test

class StringHelpersTest {
    @Test
    fun `test shift string`() {
        MatcherAssert.assertThat("12345" shift 0, Matchers.equalTo("12345"))
        MatcherAssert.assertThat("12345" shift 2, Matchers.equalTo("45123"))
        MatcherAssert.assertThat("12345" shift -2, Matchers.equalTo("34512"))
    }
}